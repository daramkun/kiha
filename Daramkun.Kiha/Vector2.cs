﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Vector2 : IEquatable<Vector2>
	{
		public static implicit operator Vector2 ( float v ) { return new Vector2 ( v ); }
		public static implicit operator Vector2 ( float [] v ) { return new Vector2 ( v ); }
		public static implicit operator float [] ( Vector2 v ) { return v.ToArray (); }

		public static readonly Vector2 Zero = new Vector2 ( 0 );
		public static readonly Vector2 One = new Vector2 ( 1 );

		public float X, Y;

		public float LengthSquared { get { return X * X + Y * Y; } }
		public float Length { get { return ( float ) Math.Sqrt ( LengthSquared ); } }

		public Vector2 ( float value ) { X = Y = value; }
		public Vector2 ( float x, float y ) { X = x; Y = y; }
		public Vector2 ( float [] xy ) { X = xy [ 0 ]; Y = xy [ 1 ]; }
		public Vector2 ( Vector3 vector )
		{
			if ( vector.Z == 0 ) { X = Y = 0; return; }
			X = vector.X / vector.Z; Y = vector.Y / vector.Z;
		}

		public Vector2 Normalize () { return Normalize ( this ); }
		public void Normalize ( out Vector2 result ) { Normalize ( ref this, out result ); }
		public static Vector2 Normalize ( Vector2 v ) { Vector2 result; Normalize ( ref v, out result ); return result; }
		public static void Normalize ( ref Vector2 v, out Vector2 result ) { result = new Vector2 ( v.X / v.Length, v.Y / v.Length ); }

		public override int GetHashCode () { return ( int ) ( Length ); }
		public override string ToString () { return String.Format ( "{{X:{0}, Y:{1}}}", X, Y ); }

		public float [] ToArray () { return new float [] { X, Y }; }

		public float this [ int index ]
		{
			get { return ( index == 0 ) ? X : ( ( index == 1 ) ? Y : float.NaN ); }
			set { switch ( index ) { case 0: X = value; break; case 1: Y = value; break; default: throw new IndexOutOfRangeException (); } }
		}

		public static Vector2 operator + ( Vector2 a, Vector2 b ) { Vector2 result; Add ( ref a, ref b, out result ); return result; }
		public static Vector2 operator - ( Vector2 a ) { Vector2 result; Negate ( ref a, out result ); return result; }
		public static Vector2 operator - ( Vector2 a, Vector2 b ) { Vector2 result; Subtract ( ref a, ref b, out result ); return result; }
		public static Vector2 operator * ( Vector2 a, Vector2 b ) { Vector2 result; Multiply ( ref a, ref b, out result ); return result; }
		public static Vector2 operator * ( Vector2 a, float b ) { Vector2 result; Multiply ( ref a, b, out result ); return result; }
		public static Vector2 operator * ( float a, Vector2 b ) { Vector2 result; Multiply ( ref b, a, out result ); return result; }
		public static Vector2 operator / ( Vector2 a, Vector2 b ) { Vector2 result; Divide ( ref a, ref b, out result ); return result; }
		public static Vector2 operator / ( Vector2 a, float b ) { Vector2 result; Divide ( ref a, b, out result ); return result; }
		public static bool operator == ( Vector2 v1, Vector2 v2 ) { return v1.X == v2.X && v1.Y == v2.Y; }
		public static bool operator != ( Vector2 v1, Vector2 v2 ) { return !( v1 == v2 ); }

		public override bool Equals ( object obj ) { if ( !( obj is Vector2 ) ) return false; return this == ( Vector2 ) obj; }
		public bool Equals ( Vector2 other ) { return Equals ( ( object ) other ); }

		public static Vector2 Add ( Vector2 a, Vector2 b ) { return a + b; }
		public static Vector2 Negate ( Vector2 b ) { return -b; }
		public static Vector2 Subtract ( Vector2 a, Vector2 b ) { return a - b; }
		public static Vector2 Multiply ( Vector2 a, Vector2 b ) { return a * b; }
		public static Vector2 Multiply ( Vector2 a, float b ) { return a * b; }
		public static Vector2 Multiply ( float a, Vector2 b ) { return b * a; }
		public static Vector2 Divide ( Vector2 a, Vector2 b ) { return a / b; }
		public static Vector2 Divide ( Vector2 a, float b ) { return a / b; }

		public static void Add ( ref Vector2 a, ref Vector2 b, out Vector2 result ) { result = new Vector2 ( a.X + b.X, a.Y + b.Y ); }
		public static void Negate ( ref Vector2 a, out Vector2 result ) { result = new Vector2 ( -a.X, -a.Y ); }
		public static void Subtract ( ref Vector2 a, ref Vector2 b, out Vector2 result ) { result = new Vector2 ( a.X - b.X, a.Y - b.Y ); }
		public static void Multiply ( ref Vector2 a, ref Vector2 b, out Vector2 result ) { result = new Vector2 ( a.X * b.X, a.Y * b.Y ); }
		public static void Multiply ( ref Vector2 a, float b, out Vector2 result ) { result = new Vector2 ( a.X * b, a.Y * b ); }
		public static void Multiply ( float a, ref Vector2 b, out Vector2 result ) { Multiply ( ref b, a, out result ); }
		public static void Divide ( ref Vector2 a, ref Vector2 b, out Vector2 result ) { result = new Vector2 ( a.X / b.X, a.Y / b.Y ); }
		public static void Divide ( ref Vector2 a, float b, out Vector2 result ) { result = new Vector2 ( a.X / b, a.Y / b ); }

		public static float Dot ( Vector2 v1, Vector2 v2 ) { float result; Dot ( ref v1, ref v2, out result ); return result; }
		public static Vector2 Cross ( Vector2 v1, float v2 ) { Vector2 result; Cross ( ref v1, v2, out result ); return result; }
		public static Vector2 Cross ( float v1, Vector2 v2 ) { Vector2 result; Cross ( ref v2, v1, out result ); return result; }
		public static Vector2 Cross ( Vector2 v1, Vector2 v2 ) { Vector2 result; Cross ( ref v1, ref v2, out result ); return result; }

		public static void Dot ( ref Vector2 v1, ref Vector2 v2, out float result ) { result = v1.X * v2.X + v1.Y * v2.Y; }
		public static void Cross ( ref Vector2 v1, float v2, out Vector2 result ) { result = new Vector2 ( v1.Y * v2, v1.X * -v2 ); }
		public static void Cross ( float v1, ref Vector2 v2, out Vector2 result ) { Cross ( ref v2, v1, out result ); }
		public static void Cross ( ref Vector2 v1, ref Vector2 v2, out Vector2 result ) { result = new Vector2 ( v1.X * v2.Y, v1.Y * v2.X ); }

		public static Vector2 Transform ( Vector2 position, Matrix4x4 matrix )
		{
			Vector2 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}
		public static void Transform ( ref Vector2 position, ref Matrix4x4 matrix, out Vector2 result )
		{
			result = new Vector2 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + matrix.M41,
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + matrix.M42
			);
		}

		public static Vector2 TransformNormal ( Vector2 normal, Matrix4x4 matrix )
		{
			Vector2 result;
			TransformNormal ( ref normal, ref matrix, out result );
			return result;
		}
		public static void TransformNormal ( ref Vector2 normal, ref Matrix4x4 matrix, out Vector2 result )
		{
			result = new Vector2 (
				( normal.X * matrix.M11 ) + ( normal.Y * matrix.M21 ),
				( normal.X * matrix.M12 ) + ( normal.Y * matrix.M22 )
			);
		}

		public static float Distance ( Vector2 v1, Vector2 v2 ) { float result; Distance ( ref v1, ref v2, out result ); return result; }
		public static float DistanceSquared ( Vector2 v1, Vector2 v2 ) { float result; DistanceSquared ( ref v1, ref v2, out result ); return result; }

		public static void Distance ( ref Vector2 v1, ref Vector2 v2, out float result ) { result = ( v2 - v1 ).Length; }
		public static void DistanceSquared ( ref Vector2 v1, ref Vector2 v2, out float result ) { result = ( v2 - v1 ).LengthSquared; }

		public static Vector2 Max ( Vector2 v1, Vector2 v2 ) { Vector2 result; Max ( ref v1, ref v2, out result ); return result; }
		public static Vector2 Min ( Vector2 v1, Vector2 v2 ) { Vector2 result; Min ( ref v1, ref v2, out result ); return result; }
		public static Vector2 Clamp ( Vector2 v1, Vector2 v2, Vector2 v3 ) { Vector2 result; Clamp ( ref v1, ref v2, ref v3, out result ); return result; }
		public static Vector2 Absolute ( Vector2 v ) { Vector2 result; Absolute ( ref v, out result ); return result; }
		public static Vector2 Barycentric ( Vector2 value1, Vector2 value2, Vector2 value3, float amount1, float amount2 )
		{
			Vector2 result;
			Barycentric ( ref value1, ref value2, ref value3, amount1, amount2, out result );
			return result;
		}
		public static Vector2 CatmullRom ( Vector2 value1, Vector2 value2, Vector2 value3, Vector2 value4, float amount )
		{
			Vector2 result;
			CatmullRom ( ref value1, ref value2, ref value3, ref value4, amount, out result );
			return result;
		}
		public static Vector2 Reflect ( Vector2 vector, Vector2 normal )
		{
			Vector2 result;
			Reflect ( ref vector, ref normal, out result );
			return result;
		}
		public static Vector2 Hermite ( Vector2 value1, Vector2 tangent1, Vector2 value2, Vector2 tangent2, float amount )
		{
			Vector2 result;
			Hermite ( ref value1, ref tangent1, ref value2, ref tangent2, amount, out result );
			return result;
		}
		public static Vector2 Lerp ( Vector2 value1, Vector2 value2, float amount )
		{
			Vector2 result;
			Lerp ( ref value1, ref value2, amount, out result );
			return result;
		}
		public static Vector2 SmoothStep ( Vector2 value1, Vector2 value2, float amount )
		{
			Vector2 result;
			SmoothStep ( ref value1, ref value2, amount, out result );
			return result;
		}

		public static void Max ( ref Vector2 v1, ref Vector2 v2, out Vector2 result ) { result = new Vector2 ( Math.Max ( v1.X, v2.X ), Math.Max ( v1.Y, v2.Y ) ); }
		public static void Min ( ref Vector2 v1, ref Vector2 v2, out Vector2 result ) { result = new Vector2 ( Math.Min ( v1.X, v2.X ), Math.Min ( v1.Y, v2.Y ) ); }
		public static void Clamp ( ref Vector2 v1, ref Vector2 v2, ref Vector2 v3, out Vector2 result ) { result = Max ( v2, Min ( v1, v3 ) ); }
		public static void Absolute ( ref Vector2 v, out Vector2 result ) { result = new Vector2 ( Math.Abs ( v.X ), Math.Abs ( v.Y ) ); }
		public static void Barycentric ( ref Vector2 value1, ref Vector2 value2, ref Vector2 value3, float amount1, float amount2, out Vector2 result )
		{
			result = new Vector2 (
				MathHelper.Barycentric ( value1.X, value2.X, value3.X, amount1, amount2 ),
				MathHelper.Barycentric ( value1.Y, value2.Y, value3.Y, amount1, amount2 )
			);
		}
		public static void CatmullRom ( ref Vector2 value1, ref Vector2 value2, ref Vector2 value3, ref Vector2 value4, float amount, out Vector2 result )
		{
			result = new Vector2 (
				MathHelper.CatmullRom ( value1.X, value2.X, value3.X, value4.X, amount ),
				MathHelper.CatmullRom ( value1.Y, value2.Y, value3.Y, value4.Y, amount )
			);
		}
		public static void Reflect ( ref Vector2 vector, ref Vector2 normal, out Vector2 result )
		{
			float val = 2.0f * ( ( vector.X * normal.X ) + ( vector.Y * normal.Y ) );
			result = new Vector2 ( vector.X - ( normal.X * val ), vector.Y - ( normal.Y * val ) );
		}
		public static void Hermite ( ref Vector2 value1, ref Vector2 tangent1, ref Vector2 value2, ref Vector2 tangent2, float amount, out Vector2 result )
		{
			result = new Vector2 (
				MathHelper.Hermite ( value1.X, tangent1.X, value2.X, tangent2.X, amount ),
				MathHelper.Hermite ( value1.Y, tangent1.Y, value2.Y, tangent2.Y, amount )
			);
		}
		public static void Lerp ( ref Vector2 value1, ref Vector2 value2, float amount, out Vector2 result )
		{
			result = new Vector2 (
				MathHelper.Lerp ( value1.X, value2.X, amount ),
				MathHelper.Lerp ( value1.Y, value2.Y, amount )
			);
		}
		public static void SmoothStep ( ref Vector2 value1, ref Vector2 value2, float amount, out Vector2 result )
		{
			result = new Vector2 (
				MathHelper.SmoothStep ( value1.X, value2.X, amount ),
				MathHelper.SmoothStep ( value1.Y, value2.Y, amount )
			);
		}
	}
}
