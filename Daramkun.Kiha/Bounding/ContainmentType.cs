﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Bounding
{
	public enum ContainmentType
	{
		Disjoint, Contains, Intersects
	}
}
