﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Quaternion : IEquatable<Quaternion>
	{
		public static implicit operator Quaternion ( float v ) { return new Quaternion ( v, v, v, v ); }
		public static implicit operator Quaternion ( float [] v ) { return new Quaternion ( v [ 0 ], v [ 1 ], v [ 2 ], v [ 3 ] ); }
		public static implicit operator float [] ( Quaternion v ) { return v.ToArray (); }

		public static readonly Quaternion Identity = new Quaternion ( 0, 0, 0, 1 );

		public float X, Y, Z, W;

		public float LengthSquared { get { return X * X + Y * Y + Z * Z + W * W; } }
		public float Length { get { return ( float ) Math.Sqrt ( LengthSquared ); } }

		public Quaternion ( float x, float y, float z, float w ) { X = x; Y = y; Z = z; W = w; }
		public Quaternion ( Vector3 vectorPart, float scalarPart ) : this ( vectorPart.X, vectorPart.Y, vectorPart.Z, scalarPart ) { }
		public Quaternion ( Vector4 vector ) : this ( vector.X, vector.Y, vector.Z, vector.W ) { }
		public Quaternion ( float yaw, float pitch, float roll )
		{
			float num9 = roll * 0.5f, num6 = ( float ) Math.Sin ( num9 ), num5 = ( float ) Math.Cos ( num9 );
			float num8 = pitch * 0.5f, num4 = ( float ) Math.Sin ( num8 ), num3 = ( float ) Math.Cos ( num8 );
			float num7 = yaw * 0.5f, num2 = ( float ) Math.Sin ( num7 ), num = ( float ) Math.Cos ( num7 );
			X = ( ( num * num4 ) * num5 ) + ( ( num2 * num3 ) * num6 );
			Y = ( ( num2 * num3 ) * num5 ) - ( ( num * num4 ) * num6 );
			Z = ( ( num * num3 ) * num6 ) - ( ( num2 * num4 ) * num5 );
			W = ( ( num * num3 ) * num5 ) + ( ( num2 * num4 ) * num6 );
		}
		public Quaternion ( Matrix4x4 rotMatrix )
		{
			float num8 = ( rotMatrix.M11 + rotMatrix.M22 ) + rotMatrix.M33;
			if ( num8 > 0f )
			{
				float num = ( float ) Math.Sqrt ( ( double ) ( num8 + 1f ) );
				num = 0.5f / num;
				X = ( rotMatrix.M23 - rotMatrix.M32 ) * num;
				Y = ( rotMatrix.M31 - rotMatrix.M13 ) * num;
				Z = ( rotMatrix.M12 - rotMatrix.M21 ) * num;
				W = num * 0.5f;
			}
			else if ( ( rotMatrix.M11 >= rotMatrix.M22 ) && ( rotMatrix.M11 >= rotMatrix.M33 ) )
			{
				float num7 = ( float ) Math.Sqrt ( ( double ) ( ( ( 1f + rotMatrix.M11 ) - rotMatrix.M22 ) - rotMatrix.M33 ) );
				float num4 = 0.5f / num7;
				X = 0.5f * num7;
				Y = ( rotMatrix.M12 + rotMatrix.M21 ) * num4;
				Z = ( rotMatrix.M13 + rotMatrix.M31 ) * num4;
				W = ( rotMatrix.M23 - rotMatrix.M32 ) * num4;
			}
			else if ( rotMatrix.M22 > rotMatrix.M33 )
			{
				float num6 = ( float ) Math.Sqrt ( ( ( 1f + rotMatrix.M22 ) - rotMatrix.M11 ) - rotMatrix.M33 );
				float num3 = 0.5f / num6;
				X = ( rotMatrix.M21 + rotMatrix.M12 ) * num3;
				Y = 0.5f * num6;
				Z = ( rotMatrix.M32 + rotMatrix.M23 ) * num3;
				W = ( rotMatrix.M31 - rotMatrix.M13 ) * num3;
			}
			else
			{
				float num5 = ( float ) Math.Sqrt ( ( double ) ( ( ( 1f + rotMatrix.M33 ) - rotMatrix.M11 ) - rotMatrix.M22 ) );
				float num2 = 0.5f / num5;
				X = ( rotMatrix.M31 + rotMatrix.M13 ) * num2;
				Y = ( rotMatrix.M32 + rotMatrix.M23 ) * num2;
				Z = 0.5f * num5;
				W = ( rotMatrix.M12 - rotMatrix.M21 ) * num2;
			}
		}

		public Quaternion Normalize () { return Normalize ( this ); }
		public void Normalize ( out Quaternion result ) { Normalize ( ref this, out result ); }
		public static Quaternion Normalize ( Quaternion v ) { Quaternion result; Normalize ( ref v, out result ); return result; }
		public static void Normalize ( ref Quaternion v, out Quaternion result )
		{ result = new Quaternion ( v.X / v.Length, v.Y / v.Length, v.Z / v.Length, v.W / v.Length ); }

		public override int GetHashCode () { return ( int ) ( Length ); }

		public override string ToString () { return string.Format ( "{{X:{0}, Y:{1}, Z:{2}, W:{3}}}", X, Y, Z, W ); }

		public Matrix4x4 ToMatrix4x4 () { Matrix4x4 result; ToMatrix4x4 ( out result ); return result; }
		public void ToMatrix4x4 ( out Matrix4x4 result ) { result = new Matrix4x4 ( this ); }
		public Vector4 ToVector4 () { Vector4 result; ToVector4 ( out result ); return result; }
		public void ToVector4 ( out Vector4 result ) { result = new Vector4 ( X, Y, Z, W ); }
		public float [] ToArray () { return new float [] { X, Y, Z, W }; }

		public float this [ int index ]
		{
			get { return ( index == 0 ) ? X : ( ( index == 1 ) ? Y : ( ( index == 2 ) ? Z : ( ( index == 3 ) ? W : float.NaN ) ) ); }
			set
			{
				switch ( index )
				{
					case 0: X = value; break;
					case 1: Y = value; break;
					case 2: Z = value; break;
					case 3: W = value; break;
					default: throw new IndexOutOfRangeException ();
				}
			}
		}

		public static Quaternion operator + ( Quaternion a, Quaternion b ) { Quaternion result; Add ( ref a, ref b, out result ); return result; }
		public static Quaternion operator - ( Quaternion a ) { Quaternion result; Negate ( ref a, out result ); return result; }
		public static Quaternion operator - ( Quaternion a, Quaternion b ) { Quaternion result; Subtract ( ref a, ref b, out result ); return result; }
		public static Quaternion operator * ( Quaternion a, Quaternion b ) { Quaternion result; Multiply ( ref a, ref b, out result ); return result; }
		public static Quaternion operator * ( Quaternion a, float b ) { Quaternion result; Multiply ( ref a, b, out result ); return result; }
		public static Quaternion operator * ( float a, Quaternion b ) { Quaternion result; Multiply ( ref b, a, out result ); return result; }
		public static Quaternion operator / ( Quaternion a, Quaternion b ) { Quaternion result; Divide ( ref a, ref b, out result ); return result; }
		public static Quaternion operator / ( Quaternion a, float b ) { Quaternion result; Divide ( ref a, b, out result ); return result; }
		public static Quaternion operator ~ ( Quaternion a ) { Quaternion result; Invert ( ref a, out result ); return result; }

		public static bool operator == ( Quaternion a, Quaternion b ) { return a.Equals ( b ); }
		public static bool operator != ( Quaternion a, Quaternion b ) { return !( a == b ); }

		public override bool Equals ( object obj )
		{
			if ( !( obj is Quaternion ) ) return false;
			Quaternion q = ( Quaternion ) obj;
			return X == q.X && Y == q.Y && Z == q.Z && W == q.W;
		}
		public bool Equals ( Quaternion other ) { return Equals ( ( object ) other ); }

		public static Quaternion Add ( Quaternion a, Quaternion b ) { return a + b; }
		public static Quaternion Subtract ( Quaternion a, Quaternion b ) { return a - b; }
		public static Quaternion Multiply ( Quaternion a, Quaternion b ) { return a * b; }
		public static Quaternion Multiply ( Quaternion a, float b ) { return a * b; }
		public static Quaternion Multiply ( float a, Quaternion b ) { return b * a; }
		public static Quaternion Dividie ( Quaternion a, Quaternion b ) { return a / b; }
		public static Quaternion Divide ( Quaternion a, float b ) { return a / b; }
		public static Quaternion Invert ( Quaternion a ) { return ~a; }

		public static void Add ( ref Quaternion a, ref Quaternion b, out Quaternion result )
		{ result = new Quaternion ( a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W ); }
		public static void Negate ( ref Quaternion a, out Quaternion result )
		{ result = new Quaternion ( -a.X, -a.Y, -a.Z, -a.W ); }
		public static void Subtract ( ref Quaternion a, ref Quaternion b, out Quaternion result )
		{ result = new Quaternion ( a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.W - b.W ); }
		public static void Multiply ( ref Quaternion a, ref Quaternion b, out Quaternion result )
		{
			float x = a.X, y = a.Y, z = a.Z, w = a.W;
			float num4 = b.X, num3 = b.Y, num2 = b.Z, num1 = b.W;
			float num12 = ( y * num2 ) - ( z * num3 ), num11 = ( z * num4 ) - ( x * num2 ),
				num10 = ( x * num3 ) - ( y * num4 ), num9 = ( ( x * num4 ) + ( y * num3 ) ) + ( z * num2 );
			result = new Quaternion ( ( ( x * num1 ) + ( num4 * w ) ) + num12, ( ( y * num1 ) + ( num3 * w ) ) + num11,
				( ( z * num1 ) + ( num2 * w ) ) + num10, ( w * num1 ) - num9 );
		}
		public static void Multiply ( ref Quaternion a, float b, out Quaternion result )
		{ result = new Quaternion ( a.X * b, a.Y * b, a.Z * b, a.W * b ); }
		public static void Multiply ( float a, ref Quaternion b, out Quaternion result ) { Multiply ( ref b, a, out result ); }
		public static void Divide ( ref Quaternion a, ref Quaternion b, out Quaternion result )
		{
			float x = a.X, y = a.Y, z = a.Z, w = a.W;
			float num14 = ( ( ( b.X * b.X ) + ( b.Y * b.Y ) ) + ( b.Z * b.Z ) ) + ( b.W * b.W );
			float num5 = 1f / num14, num4 = -b.X * num5, num3 = -b.Y * num5, num2 = -b.Z * num5, num1 = b.W * num5;
			float num13 = ( y * num2 ) - ( z * num3 ), num12 = ( z * num4 ) - ( x * num2 );
			float num11 = ( x * num3 ) - ( y * num4 ), num10 = ( ( x * num4 ) + ( y * num3 ) ) + ( z * num2 );
			result = new Quaternion ( ( ( x * num1 ) + ( num4 * w ) ) + num13, ( ( y * num1 ) + ( num3 * w ) ) + num12,
				( ( z * num1 ) + ( num2 * w ) ) + num11, ( w * num1 ) - num10 );
		}
		public static void Divide ( ref Quaternion a, float b, out Quaternion result )
		{ result = new Quaternion ( a.X / b, a.Y / b, a.Z / b, a.W / b ); }
		public static void Invert ( ref Quaternion a, out Quaternion result )
		{ result = new Quaternion ( -a.X / a.LengthSquared, -a.Y / a.LengthSquared, -a.Z / a.LengthSquared, a.W / a.LengthSquared ); }

		public static float Dot ( Quaternion a, Quaternion b ) { float result; Dot ( ref a, ref b, out result ); return result; }
		public static void Dot ( ref Quaternion a, ref Quaternion b, out float result )
		{
			result = ( ( ( ( a.X * b.X ) + ( a.Y * b.Y ) ) + ( a.Z * b.Z ) ) + ( a.W * b.W ) );
		}

		public static Quaternion Conjugate ( Quaternion v ) { Quaternion result; Conjugate ( ref v, out result ); return result; }
		public static Quaternion Concatenate ( Quaternion v1, Quaternion v2 ) { Quaternion result; Concatenate ( ref v1, ref v2, out result ); return result; }
		public static Quaternion Lerp ( Quaternion v1, Quaternion v2, float amount ) { Quaternion result; Lerp ( ref v1, ref v2, amount, out result ); return result; }
		public static Quaternion Slerp ( Quaternion v1, Quaternion v2, float amount ) { Quaternion result; Slerp ( ref v1, ref v2, amount, out result ); return result; }

		public static void Conjugate ( ref Quaternion v, out Quaternion result ) { result = new Quaternion ( -v.X, -v.Y, -v.Z, v.W ); }
		public static void Concatenate ( ref Quaternion v1, ref Quaternion v2, out Quaternion result )
		{
			float x = v2.X;
			float y = v2.Y;
			float z = v2.Z;
			float w = v2.W;
			float num4 = v1.X;
			float num3 = v1.Y;
			float num2 = v1.Z;
			float num = v1.W;
			float num12 = ( y * num2 ) - ( z * num3 );
			float num11 = ( z * num4 ) - ( x * num2 );
			float num10 = ( x * num3 ) - ( y * num4 );
			float num9 = ( ( x * num4 ) + ( y * num3 ) ) + ( z * num2 );
			result = new Quaternion (
				( ( x * num ) + ( num4 * w ) ) + num12,
				( ( y * num ) + ( num3 * w ) ) + num11,
				( ( z * num ) + ( num2 * w ) ) + num10,
				( w * num ) - num9
			);
		}
		public static void Lerp ( ref Quaternion v1, ref Quaternion v2, float amount, out Quaternion result )
		{
			float num = amount;
			float num2 = 1f - num;
			float num5 = ( ( ( v1.X * v2.X ) + ( v1.Y * v2.Y ) ) + ( v1.Z * v2.Z ) ) + ( v1.W * v2.W );
			if ( num5 >= 0f )
				result = new Quaternion (
					( num2 * v1.X ) + ( num * v2.X ),
					( num2 * v1.Y ) + ( num * v2.Y ),
					( num2 * v1.Z ) + ( num * v2.Z ),
					( num2 * v1.W ) + ( num * v2.W )
				);
			else
				result = new Quaternion (
					( num2 * v1.X ) - ( num * v2.X ),
					( num2 * v1.Y ) - ( num * v2.Y ),
					( num2 * v1.Z ) - ( num * v2.Z ),
					( num2 * v1.W ) - ( num * v2.W )
				);
			float num4 = ( ( ( result.X * result.X ) + ( result.Y * result.Y ) ) + ( result.Z * result.Z ) ) + ( result.W * result.W );
			float num3 = 1f / ( ( float ) Math.Sqrt ( ( double ) num4 ) );
			result *= num3;
		}
		public static void Slerp ( ref Quaternion v1, ref Quaternion v2, float amount, out Quaternion result )
		{
			float num2, num3, num = amount;
			float num4 = ( ( ( v1.X * v2.X ) + ( v1.Y * v2.Y ) ) + ( v1.Z * v2.Z ) ) + ( v1.W * v2.W );
			bool flag = false;
			if ( num4 < 0f ) { flag = true; num4 = -num4; }
			if ( num4 > 0.999999f ) { num3 = 1f - num; num2 = flag ? -num : num; }
			else
			{
				float num5 = ( float ) Math.Acos ( ( double ) num4 );
				float num6 = ( float ) ( 1.0 / Math.Sin ( ( double ) num5 ) );
				num3 = ( ( float ) Math.Sin ( ( double ) ( ( 1f - num ) * num5 ) ) ) * num6;
				num2 = flag ? ( ( ( float ) -Math.Sin ( ( double ) ( num * num5 ) ) ) * num6 ) : ( ( ( float ) Math.Sin ( ( double ) ( num * num5 ) ) ) * num6 );
			}
			result = new Quaternion (
				( num3 * v1.X ) + ( num2 * v2.X ),
				( num3 * v1.Y ) + ( num2 * v2.Y ),
				( num3 * v1.Z ) + ( num2 * v2.Z ),
				( num3 * v1.W ) + ( num2 * v2.W )
			);
		}
	}
}
