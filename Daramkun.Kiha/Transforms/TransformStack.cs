﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class TransformStack : ITransform
	{
		List<Matrix4x4> stack = new List<Matrix4x4> ();
		Matrix4x4 transformCached;
		bool isChanged;

		public int PushLevel { get { return stack.Count; } }

		public TransformStack () { stack.Add ( Matrix4x4.Identity ); isChanged = true; }

		public void PushMatrix ()
		{
			stack.Add ( Matrix4x4.Identity );
			isChanged = true;
		}

		public void PopMatrix ()
		{
			if ( stack.Count != 1 )
				stack.RemoveAt ( stack.Count - 1 );
			isChanged = true;
		}

		public void SetIdentity ()
		{
			stack [ stack.Count - 1 ] = Matrix4x4.Identity;
			isChanged = true;
		}

		public void Multiply ( ref Matrix4x4 matrix )
		{
			Matrix4x4 top = stack [ stack.Count - 1 ];
			Matrix4x4.Multiply ( ref top, ref matrix, out top );
			stack [ stack.Count - 1 ] = top;
			isChanged = true;
		}

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				transformCached = Matrix4x4.Identity;
				for ( int i = 0; i < stack.Count; ++i )
				{
					Matrix4x4 matrix = stack [ i ];
					Matrix4x4.Multiply ( ref transformCached, ref matrix, out transformCached );
				}
				isChanged = false;
			}
			result = transformCached;
		}
	}
}
