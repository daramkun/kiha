﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class World2 : ITransform
	{
		World3 world;

		public Vector2? Translate
		{
			get { return ( world.Translate != null ) ? new Vector2 ( world.Translate.Value.X, world.Translate.Value.Y ) : null; }
			set { world.Translate = value == null ? null : new Vector3 ( value.Value.X, value.Value.Y, 0 ); }
		}
		public Vector2? Scale
		{
			get { return ( world.Scale != null ) ? new Vector2 ( world.Scale.Value.X, world.Scale.Value.Y ) : null; }
			set { world.Scale = value == null ? null : new Vector3 ( value.Value.X, value.Value.Y, 0 ); }
		}

		public Vector2? RotationCenter
		{
			get { return ( world.RotationCenter != null ) ? new Vector2 ( world.RotationCenter.Value.X, world.RotationCenter.Value.Y ) : null; }
			set { world.RotationCenter = value == null ? null : new Vector3 ( value.Value.X, value.Value.Y, 0 ); }
		}
		public Vector2? ScaleCenter
		{
			get { return ( world.ScaleCenter != null ) ? new Vector2 ( world.ScaleCenter.Value.X, world.ScaleCenter.Value.Y ) : null; }
			set { world.ScaleCenter = value == null ? null : new Vector3 ( value.Value.X, value.Value.Y, 0 ); }
		}

		public float Rotation
		{
			get { return ( world.Rotation != null ) ? world.Rotation.Value.Z : 0; }
			set { world.Rotation = new Vector3 ( 0, 0, value ); }
		}

		public World2 () { world = new World3 (); }

		public void GetMatrix ( out Matrix4x4 result )
		{
			world.GetMatrix ( out result );
		}
	}
}
