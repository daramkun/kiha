﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public interface ITransform
	{
		void GetMatrix ( out Matrix4x4 result );
	}
}
