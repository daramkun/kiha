﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class Camera3 : IHandDirectionTransform
	{
		LookAt lookAt;
		Vector3 position;
		Vector3 yawPitchRoll;
		bool isFlyingMode;
		bool isChanged;
		Vector3 move;
		Matrix4x4 transformCached;

		public Vector3 Position { get { return position; } set { position = value; isChanged = true; } }
		public float Yaw { get { return yawPitchRoll.X; } set { yawPitchRoll.X = value; isChanged = true; } }
		public float Pitch { get { return yawPitchRoll.Y; } set { yawPitchRoll.Y = value; isChanged = true; } }
		public float Roll { get { return yawPitchRoll.Z; } set { yawPitchRoll.Z = value; isChanged = true; } }
		public bool IsFlyingMode { get { return isFlyingMode; } set { isFlyingMode = value; } }

		public HandDirection HandDirection
		{
			get { return lookAt.HandDirection; }
			set { lookAt.HandDirection = value; }
		}

		public Camera3 ()
		{
			lookAt = new LookAt ( Vector3.Zero );
			IsFlyingMode = false;
		}

		public void Strafe ( float unit ) { move.X += unit; isChanged = true; }
		public void Fly ( float unit ) { move.Y += unit; isChanged = true; }
		public void Walk ( float unit ) { move.Z -= unit; isChanged = true; }

		public Matrix4x4 Matrix { get { if ( !isChanged ) return transformCached; else { Matrix4x4 result; GetMatrix ( out result ); return result; } } }

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				Matrix4x4 tempMatrix;
				Vector3 tempVector;

				Vector3 ypr = new Vector3 ( yawPitchRoll.X, ( isFlyingMode ) ? yawPitchRoll.Y : 0, 0 );
				RawTransform.FromYawPitchRoll ( ref ypr, out tempMatrix );
				Vector3.Transform ( ref move, ref tempMatrix, out tempVector );
				lookAt.Position += tempVector;

				RawTransform.FromYawPitchRoll ( ref yawPitchRoll, out tempMatrix );

				Vector3 targetVector = new Vector3 ( 0, 0, -1 ), upVector = new Vector3 ( 0, 1, 0 );
				Vector3.Transform ( ref targetVector, ref tempMatrix, out tempVector );
				lookAt.Target = tempVector + lookAt.Position;
				Vector3.Transform ( ref upVector, ref tempMatrix, out tempVector );
				lookAt.UpVector = tempVector;

				lookAt.GetMatrix ( out transformCached );

				move = Vector3.Zero;
			}
			result = transformCached;
		}
	}
}
