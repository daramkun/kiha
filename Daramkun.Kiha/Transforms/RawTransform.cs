﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public static class RawTransform
	{
		#region Rotation
		public static void RotationX ( float angle, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
				1, 0, 0, 0,
				0, MathHelper.Cos(angle), MathHelper.Sin(angle), 0,
				0, -MathHelper.Sin(angle), MathHelper.Cos(angle), 0,
				0, 0, 0, 1
				);
		}
		public static void RotationY ( float angle, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
				MathHelper.Cos ( angle ), 0, -MathHelper.Sin ( angle ), 0,
				0, 1, 0, 0,
				MathHelper.Sin ( angle ), 0, MathHelper.Cos ( angle ), 0,
				0, 0, 0, 1
				);
		}
		public static void RotationZ ( float angle, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
				MathHelper.Cos ( angle ), MathHelper.Sin ( angle ), 0, 0,
				-MathHelper.Sin ( angle ), MathHelper.Cos ( angle ), 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
				);
		}
		public static void FromAxisAngle ( float x, float y, float z, float angle, out Matrix4x4 result )
		{
			float num2 = ( float ) Math.Sin ( angle );
			float num = ( float ) Math.Cos ( angle );
			float num11 = x * x;
			float num10 = y * y;
			float num9 = z * z;
			float num8 = x * y;
			float num7 = x * z;
			float num6 = y * z;

			result = new Matrix4x4
			(
				num11 + ( num * ( 1 - num11 ) ), ( num8 - ( num * num8 ) ) + ( num2 * z ), ( num7 - ( num * num7 ) ) - ( num2 * y ), 0,
				( num8 - ( num * num8 ) ) - ( num2 * z ), num10 + ( num * ( 1f - num10 ) ), ( num6 - ( num * num6 ) ) + ( num2 * x ), 0,
				( num7 - ( num * num7 ) ) + ( num2 * y ), ( num6 - ( num * num6 ) ) - ( num2 * x ), num9 + ( num * ( 1f - num9 ) ), 0,
				0, 0, 0, 1
			);
		}
		public static void FromYawPitchRoll ( ref Vector3 ypr, out Matrix4x4 result )
		{
			new Quaternion ( ypr.X, ypr.Y, ypr.Z ).ToMatrix4x4 ( out result );
		}

		public static Matrix4x4 RotationX ( float angle ) { Matrix4x4 result; RotationX ( angle, out result ); return result; }
		public static Matrix4x4 RotationY ( float angle ) { Matrix4x4 result; RotationY ( angle, out result ); return result; }
		public static Matrix4x4 RotationZ ( float angle ) { Matrix4x4 result; RotationZ ( angle, out result ); return result; }
		public static Matrix4x4 FromAxisAngle ( float x, float y, float z, float angle )
		{ Matrix4x4 result; FromAxisAngle ( x, y, z, angle, out result ); return result; }
		public static Matrix4x4 FromYawPitchRoll ( Vector3 ypr ) { Matrix4x4 result; FromYawPitchRoll ( ref ypr, out result ); return result; }
		#endregion

		#region Scale
		public static void Scale ( ref Vector3 scale, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					scale.X, 0, 0, 0,
					0, scale.Y, 0, 0,
					0, 0, scale.Z, 0,
					0, 0, 0, 1
				);
		}

		public static Matrix4x4 Scale ( Vector3 scale ) { Matrix4x4 result; Scale ( ref scale, out result ); return result; }
		#endregion

		#region Translate
		public static void Translate ( ref Vector3 translate, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					translate.X, translate.Y, translate.Z, 1
				);
		}

		public static Matrix4x4 Translate ( Vector3 translate ) { Matrix4x4 result; Translate ( ref translate, out result ); return result; }
		#endregion

		#region View
		public static void LookAtLH ( ref Vector3 eye, ref Vector3 at, ref Vector3 up, out Matrix4x4 result )
		{
			Vector3 zaxis = ( at - eye ).Normalize ();
			Vector3 xaxis = Vector3.Cross ( up, zaxis ).Normalize ();
			Vector3 yaxis = Vector3.Cross ( zaxis, xaxis );
			result = new Matrix4x4 (
					xaxis.X, yaxis.X, zaxis.X, 0,
					xaxis.Y, yaxis.Y, zaxis.Y, 0,
					xaxis.Z, yaxis.Z, zaxis.Z, 0,
					-Vector3.Dot ( xaxis, eye ), -Vector3.Dot ( yaxis, eye ), -Vector3.Dot ( zaxis, eye ), 1
				);
		}
		public static void LookAtRH ( ref Vector3 eye, ref Vector3 at, ref Vector3 up, out Matrix4x4 result )
		{
			Vector3 zaxis = ( eye - at ).Normalize ();
			Vector3 xaxis = Vector3.Cross ( up, zaxis ).Normalize ();
			Vector3 yaxis = Vector3.Cross ( zaxis, xaxis );
			result = new Matrix4x4 (
					xaxis.X, yaxis.X, zaxis.X, 0,
					xaxis.Y, yaxis.Y, zaxis.Y, 0,
					xaxis.Z, yaxis.Z, zaxis.Z, 0,
					-Vector3.Dot ( xaxis, eye ), -Vector3.Dot ( yaxis, eye ), -Vector3.Dot ( zaxis, eye ), 1 
				);
		}

		public static Matrix4x4 LookAtLH ( Vector3 eye, Vector3 at, Vector3 up )
		{ Matrix4x4 result; LookAtLH ( ref eye, ref at, ref up, out result ); return result; }
		public static Matrix4x4 LookAtRH ( Vector3 eye, Vector3 at, Vector3 up )
		{ Matrix4x4 result; LookAtRH ( ref eye, ref at, ref up, out result ); return result; }
		#endregion

		#region Perspective Projection
		public static void PerspectiveFieldOfViewLH ( float fov, float aspectRatio, float zNear, float zFar, out Matrix4x4 result )
		{
			float yScale = ( float ) ( Math.Cos ( fov / 2 ) / Math.Sin ( fov / 2 ) ),
				  xScale = yScale / aspectRatio;
			result = new Matrix4x4 (
					xScale, 0, 0, 0,
					0, yScale, 0, 0,
					0, 0, zFar / ( zFar - zNear ), 1,
					0, 0, -zNear * zFar / ( zFar - zNear ), 0
				);
		}
		public static void PerspectiveFieldOfViewRH ( float fov, float aspectRatio, float zNear, float zFar, out Matrix4x4 result )
		{
			float yScale = ( float ) ( Math.Cos ( fov / 2 ) / Math.Sin ( fov / 2 ) ),
				  xScale = yScale / aspectRatio;
			result = new Matrix4x4 (
					xScale, 0, 0, 0,
					0, yScale, 0, 0,
					0, 0, zFar / ( zNear - zFar ), 1,
					0, 0, -zNear * zFar / ( zNear - zFar ), 0
				);
		}
		public static void PerspectiveLH ( float w, float h, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 * zNear / w, 0, 0, 0,
					0, 2 * zNear / h, 0, 0,
					0, 0, zFar / ( zFar - zNear ), 1,
					0, 0, zNear * zFar / ( zFar - zNear ), 0
				);
		}
		public static void PerspectiveRH ( float w, float h, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 * zNear / w, 0, 0, 0,
					0, 2 * zNear / h, 0, 0,
					0, 0, zFar / ( zNear - zFar ), 1,
					0, 0, zNear * zFar / ( zNear - zFar ), 0
				);
		}
		public static void PerspectiveOffCenterLH ( float l, float r, float b, float t, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 * zNear / ( r - l ), 0, 0, 0,
					0, 2 * zNear / ( t - b ), 0, 0,
					( l + r ) / ( l - r ), ( t + b ) / ( b - t ), zFar / ( zFar - zNear ), 1,
					0, 0, zNear * zFar / ( zFar - zNear ), 0
				);
		}
		public static void PerspectiveOffCenterRH ( float l, float r, float b, float t, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 * zNear / ( r - l ), 0, 0, 0,
					0, 2 * zNear / ( t - b ), 0, 0,
					( l + r ) / ( l - r ), ( t + b ) / ( t - b ), zFar / ( zNear - zFar ), 1,
					0, 0, zNear * zFar / ( zNear - zFar ), 0
				);
		}

		public static Matrix4x4 PerspectiveFieldOfViewLH ( float fov, float aspectRatio, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveFieldOfViewLH ( fov, aspectRatio, zNear, zFar, out result ); return result; }
		public static Matrix4x4 PerspectiveFieldOfViewRH ( float fov, float aspectRatio, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveFieldOfViewRH ( fov, aspectRatio, zNear, zFar, out result ); return result; }
		public static Matrix4x4 PerspectiveLH ( float w, float h, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveLH ( w, h, zNear, zFar, out result ); return result; }
		public static Matrix4x4 PerspectiveRH ( float w, float h, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveRH ( w, h, zNear, zFar, out result ); return result; }
		public static Matrix4x4 PerspectiveOffCenterLH ( float l, float r, float b, float t, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveOffCenterLH ( l, r, b, t, zNear, zFar, out result ); return result; }
		public static Matrix4x4 PerspectiveOffCenterRH ( float l, float r, float b, float t, float zNear, float zFar )
		{ Matrix4x4 result; PerspectiveOffCenterRH ( l, r, b, t, zNear, zFar, out result ); return result; }
		#endregion

		#region Orthographic Projection
		public static void OrthographicOffCenterLH ( float l, float r, float b, float t, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 / ( r - l ), 0, 0, 0,
					0, 2 / ( t - b ), 0, 0,
					0, 0, 1 / ( zFar - zNear ), 0,
					( l + r ) / ( l - r ), ( t + b ) / ( b - t ), -zNear / ( zFar - zNear ), 1
				);
		}
		public static void OrthographicOffCenterRH ( float l, float r, float b, float t, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 / ( r - l ), 0, 0, 0,
					0, 2 / ( t - b ), 0, 0,
					0, 0, 1 / ( zNear - zFar ), 0,
					( l + r ) / ( l - r ), ( t + b ) / ( b - t ), -zNear / ( zNear - zFar ), 1
				);
		}
		public static void OrthographicLH ( float w, float h, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 / w, 0, 0, 0,
					0, 2 / h, 0, 0,
					0, 0, 1 / ( zFar - zNear ), 0,
					0, 0, -zNear / ( zFar - zNear ), 1
				);
		}
		public static void OrthographicRH ( float w, float h, float zNear, float zFar, out Matrix4x4 result )
		{
			result = new Matrix4x4 (
					2 / w, 0, 0, 0,
					0, 2 / h, 0, 0,
					0, 0, 1 / ( zNear - zFar ), 0,
					0, 0, -zNear / ( zNear - zFar ), 1
				);
		}

		public static Matrix4x4 OrthographicOffCenterLH ( float l, float r, float b, float t, float zNear, float zFar )
		{ Matrix4x4 result; OrthographicOffCenterLH ( l, r, b, t, zNear, zFar, out result ); return result; }
		public static Matrix4x4 OrthographicOffCenterRH ( float l, float r, float b, float t, float zNear, float zFar )
		{ Matrix4x4 result; OrthographicOffCenterRH ( l, r, b, t, zNear, zFar, out result ); return result; }
		public static Matrix4x4 OrthographicLH ( float w, float h, float zNear, float zFar )
		{ Matrix4x4 result; OrthographicLH ( w, h, zNear, zFar, out result ); return result; }
		public static Matrix4x4 OrthographicRH ( float w, float h, float zNear, float zFar )
		{ Matrix4x4 result; OrthographicRH ( w, h, zNear, zFar, out result ); return result; }
		#endregion
	}
}
