﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public static class TransformHelper
	{
		public static Matrix4x4 GetMatrix ( this ITransform transform ) { Matrix4x4 result; transform.GetMatrix ( out result ); return result; }
	}
}
