﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public interface IHandDirectionTransform : ITransform
	{
		HandDirection HandDirection { get; set; }
	}
}
