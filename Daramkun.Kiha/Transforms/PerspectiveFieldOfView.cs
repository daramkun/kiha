﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class PerspectiveFieldOfView : IHandDirectionTransform
	{
		HandDirection handDirection;
		float fov, aspect, zn, zf;
		bool isChanged;
		Matrix4x4 transformCached;

		private delegate void PerspectiveFieldOfViewDelegate ( float fov, float aspect, float zn, float zf, out Matrix4x4 res );
		PerspectiveFieldOfViewDelegate perspectiveFieldOfView;

		public HandDirection HandDirection
		{
			get { return handDirection; }
			set
			{
				handDirection = value;
				if ( handDirection == Transforms.HandDirection.RightHand )
					perspectiveFieldOfView = RawTransform.PerspectiveFieldOfViewRH;
				else perspectiveFieldOfView = RawTransform.PerspectiveFieldOfViewLH;
			}
		}

		public float FieldOfView { get { return fov; } set { fov = value; isChanged = true; } }
		public float AspectRatio { get { return aspect; } set { aspect = value; isChanged = true; } }
		public float ZNear { get { return zn; } set { zn = value; isChanged = true; } }
		public float ZFar { get { return zf; } set { zf = value; isChanged = true; } }

		public PerspectiveFieldOfView ( float fieldOfView, float aspectRatio, float zNear, float zFar )
		{
			fov = fieldOfView;
			aspect = aspectRatio;
			zn = zNear;
			zf = zFar;

			isChanged = true;
			HandDirection = HandDirection.RightHand;
		}

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				perspectiveFieldOfView ( fov, aspect, zn, zf, out transformCached );
				isChanged = false;
			}
			result = transformCached;
		}
	}
}
