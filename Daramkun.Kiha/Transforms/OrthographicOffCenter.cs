﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daramkun.Kiha.Shape;

namespace Daramkun.Kiha.Transforms
{
	public class OrthographicOffCenter : IHandDirectionTransform
	{
		HandDirection handDirection;
		Rectangle offCenter;
		float zn, zf;
		bool isChanged;
		Matrix4x4 transformCached;

		private delegate void OrthographicOffCenterDelegate ( float l, float r, float b, float t, float zn, float zf, out Matrix4x4 res );
		OrthographicOffCenterDelegate orthographicOffCenter;

		public HandDirection HandDirection
		{
			get { return handDirection; }
			set
			{
				handDirection = value;
				if ( handDirection == Transforms.HandDirection.RightHand )
					orthographicOffCenter = RawTransform.OrthographicOffCenterRH;
				else orthographicOffCenter = RawTransform.OrthographicOffCenterLH;
			}
		}

		public Rectangle OffCenter { get { return offCenter; } set { offCenter = value; isChanged = true; } }
		public float ZNear { get { return zn; } set { zn = value; isChanged = true; } }
		public float ZFar { get { return zf; } set { zf = value; isChanged = true; } }

		public OrthographicOffCenter ( Rectangle offCenter, float zNear, float zFar )
		{
			this.offCenter = offCenter;
			zn = zNear;
			zf = zFar;

			isChanged = true;
			HandDirection = HandDirection.RightHand;
		}

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				orthographicOffCenter ( offCenter.X, offCenter.Y, offCenter.X + offCenter.Width, offCenter.Y + offCenter.Height, zn, zf, out transformCached );
				isChanged = false;
			}
			result = transformCached;
		}
	}
}
