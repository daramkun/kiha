﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class LookAt : IHandDirectionTransform
	{
		HandDirection handDirection;
		Vector3 position, target, upVector;
		bool isChanged;
		Matrix4x4 transformCached;

		private delegate void LookAtDelegate ( ref Vector3 pos, ref Vector3 tar, ref Vector3 up, out Matrix4x4 res );
		LookAtDelegate lookAt;

		public HandDirection HandDirection
		{
			get { return handDirection; }
			set
			{
				handDirection = value;
				if ( handDirection == Transforms.HandDirection.RightHand )
					lookAt = RawTransform.LookAtRH;
				else lookAt = RawTransform.LookAtLH;
			}
		}

		public Vector3 Position { get { return position; } set { position = value; isChanged = true; } }
		public Vector3 Target { get { return target; } set { target = value; isChanged = true; } }
		public Vector3 UpVector { get { return upVector; } set { upVector = value; isChanged = true; } }

		public LookAt ( Vector3 position, Vector3 target, Vector3 upVector )
		{
			Position = position;
			Target = target;
			UpVector = upVector;
			isChanged = true;
			HandDirection = HandDirection.RightHand;
		}
		public LookAt ( Vector3 position, Vector3 target )
			: this ( position, target, new Vector3 ( 0, 1, 0 ) ) { }
		public LookAt ( Vector3 position )
			: this ( position, new Vector3 (), new Vector3 ( 0, 1, 0 ) ) { }

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				lookAt ( ref position, ref target, ref upVector, out transformCached );
				isChanged = false;
			}
			result = transformCached;
		}
	}
}
