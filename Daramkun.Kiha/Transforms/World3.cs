﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Transforms
{
	public class World3 : ITransform
	{
		Vector3? translate, scaleCenter, scale, rotationCenter, rotation;
		bool isChanged;
		Matrix4x4 transformCached;

		public Vector3? Translate { get { return translate; } set { translate = value; isChanged = true; } }
		public Vector3? Scale { get { return scale; } set { scale = value; isChanged = true; } }
		public Vector3? Rotation { get { return rotation; } set { rotation = value; isChanged = true; } }

		public Vector3? RotationCenter { get { return rotationCenter; } set { rotationCenter = value; isChanged = true; } }
		public Vector3? ScaleCenter { get { return scaleCenter; } set { scaleCenter = value; isChanged = true; } }

		public World3 () { }

		public void GetMatrix ( out Matrix4x4 result )
		{
			if ( isChanged )
			{
				Matrix4x4 tempMatrix;
				Vector3 tempVector;
				transformCached = Matrix4x4.Identity;
				
				if ( rotationCenter != null && rotationCenter.HasValue )
				{
					tempVector = new Vector3 ( -rotationCenter.Value.X, -rotationCenter.Value.Y, 0 );
					RawTransform.Translate ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}
				if ( rotation != null && rotation.HasValue )
				{
					if ( rotation.Value.X != 0 )
					{
						RawTransform.RotationX ( rotation.Value.X, out tempMatrix );
						Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
					}
					if ( rotation.Value.Y != 0 )
					{
						RawTransform.RotationY ( rotation.Value.Y, out tempMatrix );
						Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
					}
					if ( rotation.Value.Z != 0 )
					{
						RawTransform.RotationZ ( rotation.Value.Z, out tempMatrix );
						Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
					}
				}
				if ( rotationCenter != null && rotationCenter.HasValue )
				{
					tempVector = new Vector3 ( rotationCenter.Value.X, rotationCenter.Value.Y, 0 );
					RawTransform.Translate ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}

				if ( scaleCenter != null && scaleCenter.HasValue )
				{
					tempVector = new Vector3 ( -scaleCenter.Value.X, -scaleCenter.Value.Y, 0 );
					RawTransform.Translate ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}
				if ( scale != null && scale.HasValue )
				{
					tempVector = new Vector3 ( scale.Value.X, scale.Value.Y, 0 );
					RawTransform.Scale ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}
				if ( scaleCenter != null && scaleCenter.HasValue )
				{
					tempVector = new Vector3 ( scaleCenter.Value.X, scaleCenter.Value.Y, 0 );
					RawTransform.Translate ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}

				if ( translate != null && translate.HasValue )
				{
					tempVector = new Vector3 ( translate.Value.X, translate.Value.Y, 0 );
					RawTransform.Translate ( ref tempVector, out tempMatrix );
					Matrix4x4.Multiply ( ref transformCached, ref tempMatrix, out transformCached );
				}

				isChanged = false;
			}
			result = transformCached;
		}
	}
}
