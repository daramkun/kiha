﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Vector3 : IEquatable<Vector3>
	{
		public static implicit operator Vector3 ( float v ) { return new Vector3 ( v ); }
		public static implicit operator Vector3 ( float [] v ) { return new Vector3 ( v ); }
		public static implicit operator float [] ( Vector3 v ) { return v.ToArray (); }

		public static readonly Vector3 Zero = new Vector3 ( 0 );
		public static readonly Vector3 One = new Vector3 ( 1 );

		public float X, Y, Z;

		public float LengthSquared { get { return X * X + Y * Y + Z * Z; } }
		public float Length { get { return ( float ) Math.Sqrt ( LengthSquared ); } }

		public Vector3 ( float value ) { X = Y = Z = value; }
		public Vector3 ( float x, float y, float z ) { X = x; Y = y; Z = z; }
		public Vector3 ( Vector2 vector, float z ) { X = vector.X; Y = vector.Y; Z = z; }
		public Vector3 ( float [] xyz ) { X = xyz [ 0 ]; Y = xyz [ 1 ]; Z = xyz [ 2 ]; }
		public Vector3 ( Vector4 vector )
		{
			if ( vector.W == 0 ) { X = Y = Z = 0; return; }
			X = vector.X / vector.W; Y = vector.Y / vector.W; Z = vector.Z / vector.W;
		}

		public Vector3 Normalize () { return Normalize ( this ); }
		public void Normalize ( out Vector3 result ) { Normalize ( ref this, out result ); }
		public static Vector3 Normalize ( Vector3 v ) { Vector3 result; Normalize ( ref v, out result ); return result; }
		public static void Normalize ( ref Vector3 v, out Vector3 result ) { result = new Vector3 ( v.X / v.Length, v.Y / v.Length, v.Z / v.Length ); }

		public override int GetHashCode () { return ( int ) ( Length ); }
		public override string ToString () { return String.Format ( "{{X:{0}, Y:{1}, Z:{2}}}", X, Y, Z ); }

		public float [] ToArray () { return new float [] { X, Y, Z }; }
		public Vector2 ToVector2 () { return new Vector2 ( this ); }

		public float this [ int index ]
		{
			get { return ( index == 0 ) ? X : ( ( index == 1 ) ? Y : ( ( index == 2 ) ? Z : float.NaN ) ); }
			set { switch ( index ) { case 0: X = value; break; case 1: Y = value; break; case 2: Z = value; break; default: throw new IndexOutOfRangeException (); } }
		}

		public static Vector3 operator + ( Vector3 a, Vector3 b ) { Vector3 result; Add ( ref a, ref b, out result ); return result; }
		public static Vector3 operator - ( Vector3 a ) { Vector3 result; Negate ( ref a, out result ); return result; }
		public static Vector3 operator - ( Vector3 a, Vector3 b ) { Vector3 result; Subtract ( ref a, ref b, out result ); return result; }
		public static Vector3 operator * ( Vector3 a, Vector3 b ) { Vector3 result; Multiply ( ref a, ref b, out result ); return result; }
		public static Vector3 operator * ( Vector3 a, float b ) { Vector3 result; Multiply ( ref a, b, out result ); return result; }
		public static Vector3 operator * ( float a, Vector3 b ) { Vector3 result; Multiply ( ref b, a, out result ); return result; }
		public static Vector3 operator / ( Vector3 a, Vector3 b ) { Vector3 result; Divide ( ref a, ref b, out result ); return result; }
		public static Vector3 operator / ( Vector3 a, float b ) { Vector3 result; Divide ( ref a, b, out result ); return result; }
		public static bool operator == ( Vector3 v1, Vector3 v2 ) { return ( v1.X == v2.X && v1.Y == v2.Y ) && v1.Z == v2.Z; }
		public static bool operator != ( Vector3 v1, Vector3 v2 ) { return !( v1 == v2 ); }

		public override bool Equals ( object obj ) { if ( !( obj is Vector3 ) ) return false; return this == ( Vector3 ) obj; }
		public bool Equals ( Vector3 other ) { return Equals ( ( object ) other ); }

		public static Vector3 Add ( Vector3 a, Vector3 b ) { return a + b; }
		public static Vector3 Negate ( Vector3 b ) { return -b; }
		public static Vector3 Subtract ( Vector3 a, Vector3 b ) { return a - b; }
		public static Vector3 Multiply ( Vector3 a, Vector3 b ) { return a * b; }
		public static Vector3 Multiply ( Vector3 a, float b ) { return a * b; }
		public static Vector3 Multiply ( float a, Vector3 b ) { return b * a; }
		public static Vector3 Divide ( Vector3 a, Vector3 b ) { return a / b; }
		public static Vector3 Divide ( Vector3 a, float b ) { return a / b; }

		public static void Add ( ref Vector3 a, ref Vector3 b, out Vector3 result ) { result = new Vector3 ( a.X + b.X, a.Y + b.Y, a.Z + b.Z ); }
		public static void Negate ( ref Vector3 a, out Vector3 result ) { result = new Vector3 ( -a.X, -a.Y, -a.Z ); }
		public static void Subtract ( ref Vector3 a, ref Vector3 b, out Vector3 result ) { result = new Vector3 ( a.X - b.X, a.Y - b.Y, a.Z - b.Z ); }
		public static void Multiply ( ref Vector3 a, ref Vector3 b, out Vector3 result ) { result = new Vector3 ( a.X * b.X, a.Y * b.Y, a.Z * b.Z ); }
		public static void Multiply ( ref Vector3 a, float b, out Vector3 result ) { result = new Vector3 ( a.X * b, a.Y * b, a.Z * b ); }
		public static void Multiply ( float a, ref Vector3 b, out Vector3 result ) { Multiply ( ref b, a, out result ); }
		public static void Divide ( ref Vector3 a, ref Vector3 b, out Vector3 result ) { result = new Vector3 ( a.X / b.X, a.Y / b.Y, a.Z / b.Z ); }
		public static void Divide ( ref Vector3 a, float b, out Vector3 result ) { result = new Vector3 ( a.X / b, a.Y / b, a.Z / b ); }

		public static float Dot ( Vector3 v1, Vector3 v2 ) { float result; Dot ( ref v1, ref v2, out result ); return result; }
		public static Vector3 Cross ( Vector3 v1, Vector3 v2 ) { Vector3 result; Cross ( ref v1, ref v2, out result ); return result; }

		public static void Dot ( ref Vector3 v1, ref Vector3 v2, out float result ) { result = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z; }
		public static void Cross ( ref Vector3 v1, ref Vector3 v2, out Vector3 result )
		{
			result = new Vector3 (
				v1.Y * v2.Z - v1.Z * v2.Y,
				v1.Z * v2.X - v1.X * v2.Z,
				v1.X * v2.Y - v1.Y * v2.X
			);
		}

		public static Vector3 Transform ( Vector2 position, Matrix4x4 matrix )
		{
			Vector3 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}
		public static Vector3 Transform ( Vector3 position, Matrix4x4 matrix )
		{
			Vector3 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}
		public static Vector3 Transform ( Vector3 value, Quaternion rotation )
		{
			Vector3 result;
			Transform ( ref value, ref rotation, out result );
			return result;
		}

		public static void Transform ( ref Vector2 position, ref Matrix4x4 matrix, out Vector3 result )
		{
			result = new Vector3 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + matrix.M41,
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + matrix.M42,
				( position.X * matrix.M13 ) + ( position.Y * matrix.M23 ) + matrix.M43
			);
		}
		public static void Transform ( ref Vector3 position, ref Matrix4x4 matrix, out Vector3 result )
		{
			result = new Vector3 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + ( position.Z * matrix.M31 ) + matrix.M41,
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + ( position.Z * matrix.M32 ) + matrix.M42,
				( position.X * matrix.M13 ) + ( position.Y * matrix.M23 ) + ( position.Z * matrix.M33 ) + matrix.M43
			);
		}
		public static void Transform ( ref Vector3 value, ref Quaternion rotation, out Vector3 result )
		{
			float x = 2 * ( rotation.Y * value.Z - rotation.Z * value.Y );
			float y = 2 * ( rotation.Z * value.X - rotation.X * value.Z );
			float z = 2 * ( rotation.X * value.Y - rotation.Y * value.X );
			result.X = value.X + x * rotation.W + ( rotation.Y * z - rotation.Z * y );
			result.Y = value.Y + y * rotation.W + ( rotation.Z * x - rotation.X * z );
			result.Z = value.Z + z * rotation.W + ( rotation.X * y - rotation.Y * x );
		}

		public static Vector3 TransformNormal ( Vector3 normal, Matrix4x4 matrix )
		{ Vector3 result; TransformNormal ( ref normal, ref matrix, out result ); return result; }
		public static void TransformNormal ( ref Vector3 normal, ref Matrix4x4 matrix, out Vector3 result )
		{
			result = new Vector3 (
				( normal.X * matrix.M11 ) + ( normal.Y * matrix.M21 ) + ( normal.Z * matrix.M31 ),
				( normal.X * matrix.M12 ) + ( normal.Y * matrix.M22 ) + ( normal.Z * matrix.M32 ),
				( normal.X * matrix.M13 ) + ( normal.Y * matrix.M23 ) + ( normal.Z * matrix.M33 )
			);
		}

		public static float Distance ( Vector3 v1, Vector3 v2 ) { float result; Distance ( ref v1, ref v2, out result ); return result; }
		public static float DistanceSquared ( Vector3 v1, Vector3 v2 ) { float result; DistanceSquared ( ref v1, ref v2, out result ); return result; }

		public static void Distance ( ref Vector3 v1, ref Vector3 v2, out float result ) { result = ( v2 - v1 ).Length; }
		public static void DistanceSquared ( ref Vector3 v1, ref Vector3 v2, out float result ) { result = ( v2 - v1 ).LengthSquared; }

		public static Vector3 Max ( Vector3 v1, Vector3 v2 ) { Vector3 result; Max ( ref v1, ref v2, out result ); return result; }
		public static Vector3 Min ( Vector3 v1, Vector3 v2 ) { Vector3 result; Min ( ref v1, ref v2, out result ); return result; }
		public static Vector3 Clamp ( Vector3 v1, Vector3 v2, Vector3 v3 ) { Vector3 result; Clamp ( ref v1, ref v2, ref v3, out result ); return result; }
		public static Vector3 Absolute ( Vector3 v ) { Vector3 result; Absolute ( ref v, out result ); return result; }
		public static Vector3 Barycentric ( Vector3 value1, Vector3 value2, Vector3 value3, float amount1, float amount2 )
		{
			Vector3 result;
			Barycentric ( ref value1, ref value2, ref value3, amount1, amount2, out result );
			return result;
		}
		public static Vector3 CatmullRom ( Vector3 value1, Vector3 value2, Vector3 value3, Vector3 value4, float amount )
		{
			Vector3 result;
			CatmullRom ( ref value1, ref value2, ref value3, ref value4, amount, out result );
			return result;
		}
		public static Vector3 Reflect ( Vector3 vector, Vector3 normal )
		{
			Vector3 result;
			Reflect ( ref vector, ref normal, out result );
			return result;
		}
		public static Vector3 Hermite ( Vector3 value1, Vector3 tangent1, Vector3 value2, Vector3 tangent2, float amount )
		{
			Vector3 result;
			Hermite ( ref value1, ref tangent1, ref value2, ref tangent2, amount, out result );
			return result;
		}
		public static Vector3 Lerp ( Vector3 value1, Vector3 value2, float amount )
		{
			Vector3 result;
			Lerp ( ref value1, ref value2, amount, out result );
			return result;
		}
		public static Vector3 SmoothStep ( Vector3 value1, Vector3 value2, float amount )
		{
			Vector3 result;
			SmoothStep ( ref value1, ref value2, amount, out result );
			return result;
		}

		public static void Max ( ref Vector3 v1, ref Vector3 v2, out Vector3 result ) { result = new Vector3 ( Math.Max ( v1.X, v2.X ), Math.Max ( v1.Y, v2.Y ), Math.Max ( v1.Z, v2.Z ) ); }
		public static void Min ( ref Vector3 v1, ref Vector3 v2, out Vector3 result ) { result = new Vector3 ( Math.Min ( v1.X, v2.X ), Math.Min ( v1.Y, v2.Y ), Math.Min ( v1.Z, v2.Z ) ); }
		public static void Clamp ( ref Vector3 v1, ref Vector3 v2, ref Vector3 v3, out Vector3 result ) { result = Max ( v2, Min ( v1, v3 ) ); }
		public static void Absolute ( ref Vector3 v, out Vector3 result ) { result = new Vector3 ( Math.Abs ( v.X ), Math.Abs ( v.Y ), Math.Abs ( v.Z ) ); }
		public static void Barycentric ( ref Vector3 value1, ref Vector3 value2, ref Vector3 value3, float amount1, float amount2, out Vector3 result )
		{
			result = new Vector3 (
				MathHelper.Barycentric ( value1.X, value2.X, value3.X, amount1, amount2 ),
				MathHelper.Barycentric ( value1.Y, value2.Y, value3.Y, amount1, amount2 ),
				MathHelper.Barycentric ( value1.Z, value2.Z, value3.Z, amount1, amount2 )
			);
		}
		public static void CatmullRom ( ref Vector3 value1, ref Vector3 value2, ref Vector3 value3, ref Vector3 value4, float amount, out Vector3 result )
		{
			result = new Vector3 (
				MathHelper.CatmullRom ( value1.X, value2.X, value3.X, value4.X, amount ),
				MathHelper.CatmullRom ( value1.Y, value2.Y, value3.Y, value4.Y, amount ),
				MathHelper.CatmullRom ( value1.Z, value2.Z, value3.Z, value4.Z, amount )
			);
		}
		public static void Reflect ( ref Vector3 vector, ref Vector3 normal, out Vector3 result )
		{
			float val = 2.0f * ( ( vector.X * normal.X ) + ( vector.Y * normal.Y ) );
			result = new Vector3 ( vector.X - ( normal.X * val ), vector.Y - ( normal.Y * val ), vector.Z - ( normal.Z * val ) );
		}
		public static void Hermite ( ref Vector3 value1, ref Vector3 tangent1, ref Vector3 value2, ref Vector3 tangent2, float amount, out Vector3 result )
		{
			result = new Vector3 (
				MathHelper.Hermite ( value1.X, tangent1.X, value2.X, tangent2.X, amount ),
				MathHelper.Hermite ( value1.Y, tangent1.Y, value2.Y, tangent2.Y, amount ),
				MathHelper.Hermite ( value1.Z, tangent1.Z, value2.Z, tangent2.Z, amount )
			);
		}
		public static void Lerp ( ref Vector3 value1, ref Vector3 value2, float amount, out Vector3 result )
		{
			result = new Vector3 (
				MathHelper.Lerp ( value1.X, value2.X, amount ),
				MathHelper.Lerp ( value1.Y, value2.Y, amount ),
				MathHelper.Lerp ( value1.Z, value2.Z, amount )
			);
		}
		public static void SmoothStep ( ref Vector3 value1, ref Vector3 value2, float amount, out Vector3 result )
		{
			result = new Vector3 (
				MathHelper.SmoothStep ( value1.X, value2.X, amount ),
				MathHelper.SmoothStep ( value1.Y, value2.Y, amount ),
				MathHelper.SmoothStep ( value1.Z, value2.Z, amount )
			);
		}

		public static void GetNormal ( Vector3 [] vertices, out Vector3 result )
		{
			result = new Vector3 ();
			for ( int i = 0; i < vertices.Length; ++i )
			{
				Vector3 current = vertices [ i ];
				Vector3 next = vertices [ ( i + 1 ) % vertices.Length ];

				result.X += ( current.Y - next.Y ) * ( current.Z + next.Z );
				result.Y += ( current.Z - next.Z ) * ( current.X + next.X );
				result.Z += ( current.X - next.X ) * ( current.Y + next.Y );
			}
		}
		public static Vector3 GetNormal ( Vector3 [] vertices ) { Vector3 result; GetNormal ( vertices, out result ); return result; }
	}
}
