﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Point3 : IEquatable<Point3>
	{
		public static implicit operator Vector3 ( Point3 p ) { return new Vector3 ( p.X, p.Y, p.Z ); }
		public static implicit operator Point3 ( Vector3 p ) { return new Point3 ( p ); }
		public static implicit operator Vector4 ( Point3 p ) { return new Vector4 ( p.X, p.Y, p.Z, 1 ); }
		public static implicit operator Point3 ( Vector4 p ) { return new Point3 ( p.ToVector3 () ); }

		public int X, Y, Z;

		public Point3 ( int x, int y, int z ) { X = x; Y = y; Z = z; }
		public Point3 ( Vector3 v ) : this ( ( int ) v.X, ( int ) v.Y, ( int ) v.Z ) { }

		public override string ToString () { return string.Format ( "{{X:{0}, Y:{1}, Z:{2}}}", X, Y, Z ); }

		public bool Equals ( Point3 other ) { return X == other.X && Y == other.Y && Z == other.Z; }
	}
}
