﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Size2
	{
		public static implicit operator Vector2 ( Size2 p ) { return new Vector2 ( p.Width, p.Height ); }
		public static implicit operator Size2 ( Vector2 p ) { return new Size2 ( p ); }

		public int Width, Height;

		public Size2 ( int width, int height ) { Width = width; Height = height; }
		public Size2 ( Vector2 v ) : this ( ( int ) v.X, ( int ) v.Y ) { }

		public override string ToString () { return string.Format ( "{{Width:{0}, Height:{1}}}", Width, Height ); }
	}
}
