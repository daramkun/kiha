﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Size3
	{
		public static implicit operator Vector3 ( Size3 p ) { return new Vector3 ( p.Width, p.Height, p.Depth ); }
		public static implicit operator Size3 ( Vector3 p ) { return new Size3 ( p ); }

		public int Width, Height, Depth;

		public Size3 ( int width, int height, int depth ) { Width = width; Height = height; Depth = depth; }
		public Size3 ( Vector3 v ) : this ( ( int ) v.X, ( int ) v.Y, ( int ) v.Z ) { }

		public override string ToString () { return string.Format ( "{{Width:{0}, Height:{1}, Depth:{2}}}", Width, Height, Depth ); }
	}
}
