﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha.Shape
{
	/// <summary>
	/// 원 도형
	/// </summary>
	[StructLayout ( LayoutKind.Sequential )]
	public struct Circle : IEquatable<Circle>, IIntersect<Rectangle>, IIntersect<Circle>, IIntersect<Line2>, IIntersect<Point2>
	{
		/// <summary>
		/// 원의 중심점의 위치
		/// </summary>
		public Point2 Position;
		/// <summary>
		/// 반지름
		/// </summary>
		public float Radius;

		/// <summary>
		/// 원의 중심점의 X 위치
		/// </summary>
		public int X { get { return Position.X; } set { Position.X = value; } }
		/// <summary>
		/// 원의 중심점의 Y 위치
		/// </summary>
		public int Y { get { return Position.Y; } set { Position.Y = value; } }

		/// <summary>
		/// 원 구조체 구성
		/// </summary>
		/// <param name="x">원의 중심점의 X 위치</param>
		/// <param name="y">원의 중심점의 Y 위치</param>
		/// <param name="radius">반지름</param>
		public Circle ( int x, int y, float radius )
		{
			Position = new Point2 ( x, y );
			Radius = radius;
		}

		/// <summary>
		/// 원 구조체 구성
		/// </summary>
		/// <param name="position">원의 중심점의 위치</param>
		/// <param name="radius">반지름</param>
		public Circle ( Point2 position, float radius ) { Position = position; Radius = radius; }

		public override string ToString ()
		{
			return string.Format ( "{{X: {0}, Y: {1}, Radius: {2}}}", X, Y, Radius );
		}

		/// <summary>
		/// 충돌 체크
		/// </summary>
		/// <param name="rect">사각형 도형</param>
		/// <returns>충돌 여부</returns>
		public bool Intersect ( Rectangle rect )
		{
			float [] rcp = new float [ 4 ];
			float [] temp = new float [ 2 ];
			float min;

			float x = Position.X, y = Position.Y, r = Radius;
			float left = rect.Position.X - rect.Size.Width / 2, right = rect.Position.X + rect.Size.Width / 2,
				top = rect.Position.Y - rect.Size.Height / 2, bottom = rect.Position.X + rect.Size.Height / 2;

			rcp [ 0 ] = ( float ) System.Math.Sqrt ( System.Math.Pow ( x - left, 2 ) + System.Math.Pow ( y - top, 2 ) );
			rcp [ 1 ] = ( float ) System.Math.Sqrt ( System.Math.Pow ( x - left, 2 ) + System.Math.Pow ( y - bottom, 2 ) );
			rcp [ 2 ] = ( float ) System.Math.Sqrt ( System.Math.Pow ( x - right, 2 ) + System.Math.Pow ( y - top, 2 ) );
			rcp [ 3 ] = ( float ) System.Math.Sqrt ( System.Math.Pow ( x - right, 2 ) + System.Math.Pow ( y - bottom, 2 ) );

			if ( rcp [ 0 ] < rcp [ 1 ] ) temp [ 0 ] = rcp [ 0 ];
			else temp [ 0 ] = rcp [ 1 ];
			if ( rcp [ 2 ] < rcp [ 3 ] ) temp [ 1 ] = rcp [ 2 ];
			else temp [ 1 ] = rcp [ 3 ];

			if ( temp [ 0 ] < temp [ 1 ] ) min = temp [ 0 ];
			else min = temp [ 1 ];

			if ( left <= x + r && top <= y - r && right >= x - r && bottom >= y + r ) return true;
			else if ( left <= x - r && top <= y + r && right >= x + r && bottom >= y - r ) return true;
			else if ( min <= r ) return true;
			else return false;
		}

		/// <summary>
		/// 충돌 체크
		/// </summary>
		/// <param name="obj">원 도형</param>
		/// <returns>충돌 여부</returns>
		public bool Intersect ( Circle obj )
		{
			return ( Math.Sqrt ( Math.Pow ( X - obj.X, 2 ) + Math.Pow ( Y - obj.Y, 2 ) ) <= ( obj.Radius + Radius ) );
		}

		/// <summary>
		/// 충돌 체크
		/// </summary>
		/// <param name="obj">선 도형</param>
		/// <returns>충돌 여부</returns>
		public bool Intersect ( Line2 obj )
		{
			return obj.Intersect ( this );
		}

		/// <summary>
		/// 충돌 체크
		/// </summary>
		/// <param name="obj">점</param>
		/// <returns>충돌 여부</returns>
		public bool Intersect ( Point2 obj )
		{
			return Intersect ( new Circle ( obj, 0.5f ) );
		}

		public bool Equals ( Circle other )
		{
			return X == other.X && Y == other.Y && Radius == other.Radius;
		}
	}
}
