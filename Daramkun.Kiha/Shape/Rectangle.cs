﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha.Shape
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Rectangle : IEquatable<Rectangle>, IIntersect<Rectangle>, IIntersect<Circle>, IIntersect<Line2>, IIntersect<Point2>
	{
		public Point2 Position;
		public Size2 Size;

		public int X { get { return Position.X; } set { Position.X = value; } }
		public int Y { get { return Position.Y; } set { Position.Y = value; } }
		public int Width { get { return Size.Width; } set { Size.Width = value; } }
		public int Height { get { return Size.Height; } set { Size.Height = value; } }

		public Rectangle ( int x, int y, int width, int height )
		{
			Position = new Point2 ( x, y );
			Size = new Size2 ( width, height );
		}

		public Rectangle ( Point2 position, Size2 size ) { Position = position; Size = size; }

		public Rectangle ( Point2 position1, Point2 position2 )
		{
			Position = new Point2 ( Math.Min ( position1.X, position2.X ), Math.Min ( position1.Y, position2.Y ) );
			Size = new Vector2 ( Math.Max ( position1.X, position2.X ), Math.Max ( position1.Y, position2.Y ) ) - Position;
		}

		public override string ToString ()
		{
			return string.Format ( "{{X: {0}, Y: {1}, Width: {2}, Height: {3}}}",
				X, Y, Width, Height );
		}

		public bool Intersect ( Rectangle obj )
		{
			return ( obj.X <= X + Width && X <= obj.X + obj.Width &&
				obj.Y <= Y + Height && Y <= obj.Y + obj.Height );
		}
		
		public bool Intersect ( Circle obj )
		{
			return obj.Intersect ( this );
		}

		public bool Intersect ( Line2 obj )
		{
			return obj.Intersect ( this );
		}

		public bool Intersect ( Point2 obj )
		{
			return Intersect ( new Rectangle ( obj.X, obj.Y, 1, 1 ) );
		}

		public bool Equals ( Rectangle other )
		{
			return X == other.X && Y == other.Y && Width == other.Width && Height == other.Height;
		}
	}
}
