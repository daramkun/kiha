﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daramkun.Kiha.Shape
{
	/// <summary>
	/// 충돌 체크 인터페이스
	/// </summary>
	/// <typeparam name="T">충돌 검사 타입</typeparam>
	public interface IIntersect<T> where T : struct
	{
		bool Intersect ( T obj );
	}
}
