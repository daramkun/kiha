﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha.Shape
{
	/// <summary>
	/// 선 도형
	/// </summary>
	[StructLayout ( LayoutKind.Sequential )]
	public struct Line2 : IEquatable<Line2>, IIntersect<Rectangle>, IIntersect<Circle>, IIntersect<Line2>, IIntersect<Point2>
	{
		/// <summary>
		/// 선 시작점 위치
		/// </summary>
		public Point2 Position1;
		/// <summary>
		/// 선 끝점 위치
		/// </summary>
		public Point2 Position2;

		/// <summary>
		/// 선 시작점 X 위치
		/// </summary>
		public int X1 { get { return Position1.X; } set { Position1.X = value; } }
		/// <summary>
		/// 선 시작점 Y 위치
		/// </summary>
		public int Y1 { get { return Position1.Y; } set { Position1.Y = value; } }
		/// <summary>
		/// 선 끝점 X 위치
		/// </summary>
		public int X2 { get { return Position2.X; } set { Position2.X = value; } }
		/// <summary>
		/// 선 끝점 Y 위치
		/// </summary>
		public int Y2 { get { return Position2.Y; } set { Position2.Y = value; } }

		/// <summary>
		/// 선 구조체 구성
		/// </summary>
		/// <param name="position1">선 시작점 위치</param>
		/// <param name="position2">선 끝점 위치</param>
		public Line2 ( Point2 position1, Point2 position2 ) { Position1 = position1; Position2 = position2; }
		/// <summary>
		/// 선 구조체 구성
		/// </summary>
		/// <param name="x1">선 시작점 X 위치</param>
		/// <param name="y1">선 시작점 Y 위치</param>
		/// <param name="x2">선 끝점 X 위치</param>
		/// <param name="y2">선 끝점 Y 위치</param>
		public Line2 ( int x1, int y1, int x2, int y2 ) { Position1 = new Point2 ( x1, y1 ); Position2 = new Point2 ( x2, y2 ); }

		public bool Equals ( Line2 other )
		{
			return Position1.Equals ( other.Position1 ) && Position2.Equals ( other.Position2 );
		}

		public bool Intersect ( Line2 obj )
		{
			float denom = ( ( obj.Y2 - obj.Y1 ) * ( X2 - X1 ) ) -
				( ( obj.X2 - obj.X1 ) * ( Y2 - Y1 ) );
			if ( denom == 0 ) return false;
			else
			{
				float ua = ( ( ( obj.X2 - obj.X1 ) * ( Y1 - obj.Y1 ) ) - ( ( obj.Y2 - obj.Y1 ) * ( X1 - obj.X1 ) ) ) / denom;
				float ub = ( ( ( X2 - X1 ) * ( Y1 - obj.Y1 ) ) - ( ( Y2 - Y1 ) * ( X1 - obj.X1 ) ) ) / denom;
				if ( ( ua < 0 ) || ( ua > 1 ) || ( ub < 0 ) || ( ub > 1 ) )
					return false;

				return true;
			}
		}

		public bool Intersect ( Rectangle obj )
		{
			Line2 l = new Line2 ( obj.Position, new Point2 ( obj.X, obj.Y + obj.Height ) );
			Line2 t = new Line2 ( obj.Position, new Point2 ( obj.X + obj.Width, obj.Y ) );
			Line2 b = new Line2 ( new Point2 ( obj.X, obj.Y + obj.Height ), new Point2 ( obj.X + obj.Width, obj.Y + obj.Height ) );
			Line2 r = new Line2 ( new Point2 ( obj.X + obj.Width, obj.Y ), new Point2 ( obj.X + obj.Width, obj.Y + obj.Height ) );

			if ( Intersect ( l ) || Intersect ( t ) || Intersect ( b ) || Intersect ( r ) )
				return true;
			else
				return ( obj.X <= X1 && obj.X + obj.Width >= X2 &&
					obj.Y <= Y1 && obj.Y + obj.Height >= Y2 );
		}

		public bool Intersect ( Circle obj )
		{
			Point2 localP1 = Position1 - ( Vector2 ) obj.Position;
			Point2 localP2 = Position2 - ( Vector2 ) obj.Position;

			Point2 p2minp1 = localP2 - ( Vector2 ) localP1;

			float a = ( p2minp1.X ) * ( p2minp1.X ) + ( p2minp1.Y ) * ( p2minp1.Y );
			float b = 2 * ( ( p2minp1.X * localP1.X ) + ( p2minp1.Y * localP1.Y ) );
			float c = ( localP1.X * localP1.X ) + ( localP1.Y * localP1.Y ) - ( obj.Radius * obj.Radius );
			float delta = b * b - ( 4 * a * c );

			if ( delta < 0 ) return false;
			else return true;
		}

		public bool Intersect ( Point2 obj )
		{
			return Intersect ( new Circle ( obj, 0.5f ) );
		}
	}
}
