﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Matrix2x2 : IEquatable<Matrix2x2>
	{
		public static implicit operator Matrix2x2 ( float [] v ) { return new Matrix2x2 ( v ); }
		public static implicit operator float [] ( Matrix2x2 v ) { return v.ToArray (); }

		public float M11, M12, M21, M22;

		public Vector2 Column1 { get { return new Vector2 ( M11, M12 ); } set { M11 = value.X; M12 = value.Y; } }
		public Vector2 Column2 { get { return new Vector2 ( M21, M22 ); } set { M21 = value.X; M22 = value.Y; } }

		public static readonly Matrix2x2 Identity = new Matrix2x2 (
																	1, 0,
																	0, 1
																	);

		public Matrix2x2 ( float m11, float m12, float m21, float m22 )
		{
			M11 = m11; M12 = m12;
			M21 = m21; M22 = m22;
		}
		public Matrix2x2 ( Vector2 column1, Vector2 column2 ) : this ( column1.X, column1.Y, column2.X, column2.Y ) { }
		public Matrix2x2 ( float [] matrix2x2 )
		{
			M11 = matrix2x2 [ 0 ]; M12 = matrix2x2 [ 1 ];
			M21 = matrix2x2 [ 2 ]; M22 = matrix2x2 [ 3 ];
		}

		public Vector2 Solve ( Vector2 vector )
		{
			float a11 = M11, a12 = M21, a21 = M12, a22 = M22;
			float det = a11 * a22 - a12 * a21;
			if ( det != 0.0f ) det = 1.0f / det;
			return new Vector2 ( det * ( a22 * vector.X - a12 * vector.Y ), det * ( a11 * vector.Y - a21 * vector.X ) );
		}

		public override int GetHashCode () { return ToString ().GetHashCode (); }

		public override string ToString ()
		{
			return String.Format ( "{{11:{0}, 12:{1}} {21:{2}, 22:{3}}}", M11, M12, M21, M22 );
		}

		public float [] ToArray ()
		{
			return new float []
			{
				M11, M12,
				M21, M22,
			};
		}

		public float this [ int index ]
		{
			get
			{
				switch ( index )
				{
					case 0: return M11;
					case 1: return M12;
					case 2: return M21;
					case 3: return M22;
					default: throw new IndexOutOfRangeException ();
				}
			}
			set
			{
				switch ( index )
				{
					case 0: M11 = value; break;
					case 1: M12 = value; break;
					case 2: M21 = value; break;
					case 3: M22 = value; break;
					default: throw new IndexOutOfRangeException ();
				}
			}
		}

		public float this [ int x, int y ]
		{
			get { return this [ x + ( y * 2 ) ]; }
			set { this [ x + ( y * 2 ) ] = value; }
		}

		public static Matrix2x2 operator + ( Matrix2x2 v1, Matrix2x2 v2 ) { Matrix2x2 result; Add ( ref v1, ref v2, out result ); return result; }
		public static Matrix2x2 operator - ( Matrix2x2 v1, Matrix2x2 v2 ) { Matrix2x2 result; Subtract ( ref v1, ref v2, out result ); return result; }
		public static Matrix2x2 operator * ( Matrix2x2 v1, Matrix2x2 v2 ) { Matrix2x2 result; Multiply ( ref v1, ref v2, out result ); return result; }
		public static Matrix2x2 operator * ( Matrix2x2 v1, float v2 ) { Matrix2x2 result; Multiply ( ref v1, v2, out result ); return result; }
		public static Matrix2x2 operator * ( float v1, Matrix2x2 v2 ) { Matrix2x2 result; Multiply ( ref v2, v1, out result ); return result; }
		public static Matrix2x2 operator / ( Matrix2x2 v1, Matrix2x2 v2 ) { Matrix2x2 result; Divide ( ref v1, ref v2, out result ); return result; }
		public static Matrix2x2 operator / ( Matrix2x2 v1, float v2 ) { Matrix2x2 result; Divide ( ref v1, v2, out result ); return result; }
		public static Matrix2x2 operator ! ( Matrix2x2 v1 ) { Matrix2x2 result; Transpose ( ref v1, out result ); return result; }
		public static Matrix2x2 operator ~ ( Matrix2x2 v1 ) { Matrix2x2 result; Invert ( ref v1, out result ); return result; }

		public static bool operator == ( Matrix2x2 v1, Matrix2x2 v2 ) { return v1.Equals ( v2 ); }
		public static bool operator != ( Matrix2x2 v1, Matrix2x2 v2 ) { return !v1.Equals ( v2 ); }

		public override bool Equals ( object obj )
		{
			if ( !( obj is Matrix2x2 ) ) return false;
			Matrix2x2 t = ( Matrix2x2 ) obj;
			return Column1 == t.Column1 && Column2 == t.Column2;
		}
		public bool Equals ( Matrix2x2 other ) { return Equals ( ( object ) other ); }

		public static Matrix2x2 Add ( Matrix2x2 v1, Matrix2x2 v2 ) { return v1 + v2; }
		public static Matrix2x2 Subtract ( Matrix2x2 v1, Matrix2x2 v2 ) { return v1 - v2; }
		public static Matrix2x2 Multiply ( Matrix2x2 v1, Matrix2x2 v2 ) { return v1 * v2; }
		public static Matrix2x2 Multiply ( Matrix2x2 v1, float v2 ) { return v1 * v2; }
		public static Matrix2x2 Multiply ( float v1, Matrix2x2 v2 ) { return v2 * v1; }
		public static Matrix2x2 Divide ( Matrix2x2 v1, Matrix2x2 v2 ) { return v1 / v2; }
		public static Matrix2x2 Divide ( Matrix2x2 v1, float v2 ) { return v1 / v2; }
		public static Matrix2x2 Transpose ( Matrix2x2 v1 ) { return !v1; }
		public static Matrix2x2 Invert ( Matrix2x2 v1 ) { return ~v1; }

		public static void Add ( ref Matrix2x2 v1, ref Matrix2x2 v2, out Matrix2x2 result )
		{ result = new Matrix2x2 ( v1.M11 + v2.M11, v1.M12 + v2.M12, v1.M21 + v2.M21, v1.M22 + v2.M22 ); }
		public static void Subtract ( ref Matrix2x2 v1, ref Matrix2x2 v2, out Matrix2x2 result )
		{ result = new Matrix2x2 ( v1.M11 - v2.M11, v1.M12 - v2.M12, v1.M21 - v2.M21, v1.M22 - v2.M22 ); }
		public static void Multiply ( ref Matrix2x2 v1, ref Matrix2x2 v2, out Matrix2x2 result )
		{
			result = new Matrix2x2 (
				( v1.M11 * v2.M11 ) + ( v1.M12 * v2.M21 ),
				( v1.M11 * v2.M12 ) + ( v1.M12 * v2.M22 ),
				( v1.M21 * v2.M11 ) + ( v1.M22 * v2.M21 ),
				( v1.M21 * v2.M12 ) + ( v1.M22 * v2.M22 )
			);
		}
		public static void Multiply ( ref Matrix2x2 v1, float v2, out Matrix2x2 result ) { result = new Matrix2x2 ( v1.M11 * v2, v1.M12 * v2, v1.M21 * v2, v1.M22 * v2 ); }
		public static void Multiply ( float v1, ref Matrix2x2 v2, out Matrix2x2 result ) { Multiply ( ref v2, v1, out result ); }
		public static void Divide ( ref Matrix2x2 v1, ref Matrix2x2 v2, out Matrix2x2 result )
		{ result = new Matrix2x2 ( v1.M11 / v2.M11, v1.M12 / v2.M12, v1.M21 / v2.M21, v1.M22 / v2.M22 ); }
		public static void Divide ( ref Matrix2x2 v1, float v2, out Matrix2x2 result ) { result = new Matrix2x2 ( v1.M11 / v2, v1.M12 / v2, v1.M21 / v2, v1.M22 / v2 ); }
		public static void Transpose ( ref Matrix2x2 v1, out Matrix2x2 result ) { result = new Matrix2x2 ( v1.M11, v1.M21, v1.M12, v1.M22 ); }
		public static void Invert ( ref Matrix2x2 v1, out Matrix2x2 result )
		{
			float d = 1 / ( v1.M11 * v1.M22 - v1.M12 * v1.M21 );
			result = new Matrix2x2 ( d * v1.M22, d * v1.M12, d * v1.M21, d * v1.M11 );
		}
	}

}
