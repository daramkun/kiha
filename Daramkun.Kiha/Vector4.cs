﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Vector4 : IEquatable<Vector4>
	{
		public static implicit operator Vector4 ( float v ) { return new Vector4 ( v ); }
		public static implicit operator Vector4 ( float [] v ) { return new Vector4 ( v ); }
		public static implicit operator float [] ( Vector4 v ) { return v.ToArray (); }

		public static readonly Vector4 Zero = new Vector4 ( 0 );
		public static readonly Vector4 One = new Vector4 ( 1 );

		public float X, Y, Z, W;

		public float LengthSquared { get { return X * X + Y * Y + Z * Z + W * W; } }
		public float Length { get { return ( float ) Math.Sqrt ( LengthSquared ); } }

		public Vector4 ( float value ) { X = Y = Z = W = value; }
		public Vector4 ( float x, float y, float z, float w ) { X = x; Y = y; Z = z; W = w; }
		public Vector4 ( Vector2 vector, float z, float w ) { X = vector.X; Y = vector.Y; Z = z; W = w; }
		public Vector4 ( Vector3 vector, float w ) { X = vector.X; Y = vector.Y; Z = vector.Z; W = w; }
		public Vector4 ( float [] xyzw ) { X = xyzw [ 0 ]; Y = xyzw [ 1 ]; Z = xyzw [ 2 ]; W = xyzw [ 3 ]; }

		public Vector4 Normalize () { return Normalize ( this ); }
		public void Normalize ( out Vector4 result ) { Normalize ( ref this, out result ); }
		public static Vector4 Normalize ( Vector4 v ) { Vector4 result; Normalize ( ref v, out result ); return result; }
		public static void Normalize ( ref Vector4 v, out Vector4 result ) { result = new Vector4 ( v.X / v.Length, v.Y / v.Length, v.Z / v.Length, v.W / v.Length ); }

		public override int GetHashCode () { return ( int ) ( Length ); }
		public override string ToString () { return string.Format ( "{{X:{0}, Y:{1}, Z:{2}, W:{3}}}", X, Y, Z, W ); }

		public float [] ToArray () { return new float [] { X, Y, Z, W }; }
		public Vector3 ToVector3 () { return new Vector3 ( this ); }

		public float this [ int index ]
		{
			get { return ( index == 0 ) ? X : ( ( index == 1 ) ? Y : ( ( index == 2 ) ? Z : ( ( index == 3 ) ? W : float.NaN ) ) ); }
			set
			{
				switch ( index )
				{
					case 0: X = value; break;
					case 1: Y = value; break;
					case 2: Z = value; break;
					case 3: W = value; break;
					default: throw new IndexOutOfRangeException ();
				}
			}
		}

		public static Vector4 operator + ( Vector4 a, Vector4 b ) { Vector4 result; Add ( ref a, ref b, out result ); return result; }
		public static Vector4 operator - ( Vector4 a ) { Vector4 result; Negate ( ref a, out result ); return result; }
		public static Vector4 operator - ( Vector4 a, Vector4 b ) { Vector4 result; Subtract ( ref a, ref b, out result ); return result; }
		public static Vector4 operator * ( Vector4 a, Vector4 b ) { Vector4 result; Multiply ( ref a, ref b, out result ); return result; }
		public static Vector4 operator * ( Vector4 a, float b ) { Vector4 result; Multiply ( ref a, b, out result ); return result; }
		public static Vector4 operator * ( float a, Vector4 b ) { Vector4 result; Multiply ( ref b, a, out result ); return result; }
		public static Vector4 operator / ( Vector4 a, Vector4 b ) { Vector4 result; Divide ( ref a, ref b, out result ); return result; }
		public static Vector4 operator / ( Vector4 a, float b ) { Vector4 result; Divide ( ref a, b, out result ); return result; }
		public static bool operator == ( Vector4 v1, Vector4 v2 ) { return ( v1.X == v2.X && v1.Y == v2.Y ) && v1.Z == v2.Z; }
		public static bool operator != ( Vector4 v1, Vector4 v2 ) { return !( v1 == v2 ); }

		public override bool Equals ( object obj ) { if ( !( obj is Vector4 ) ) return false; return this == ( Vector4 ) obj; }
		public bool Equals ( Vector4 other ) { return Equals ( ( object ) other ); }

		public static Vector4 Add ( Vector4 a, Vector4 b ) { return a + b; }
		public static Vector4 Negate ( Vector4 b ) { return -b; }
		public static Vector4 Subtract ( Vector4 a, Vector4 b ) { return a - b; }
		public static Vector4 Multiply ( Vector4 a, Vector4 b ) { return a * b; }
		public static Vector4 Multiply ( Vector4 a, float b ) { return a * b; }
		public static Vector4 Multiply ( float a, Vector4 b ) { return b * a; }
		public static Vector4 Divide ( Vector4 a, Vector4 b ) { return a / b; }
		public static Vector4 Divide ( Vector4 a, float b ) { return a / b; }

		public static void Add ( ref Vector4 a, ref Vector4 b, out Vector4 result ) { result = new Vector4 ( a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W ); }
		public static void Negate ( ref Vector4 a, out Vector4 result ) { result = new Vector4 ( -a.X, -a.Y, -a.Z, -a.W ); }
		public static void Subtract ( ref Vector4 a, ref Vector4 b, out Vector4 result ) { result = new Vector4 ( a.X - b.X, a.Y - b.Y, a.Z - b.Z, a.W - b.W ); }
		public static void Multiply ( ref Vector4 a, ref Vector4 b, out Vector4 result ) { result = new Vector4 ( a.X * b.X, a.Y * b.Y, a.Z * b.Z, a.W * b.W ); }
		public static void Multiply ( ref Vector4 a, float b, out Vector4 result ) { result = new Vector4 ( a.X * b, a.Y * b, a.Z * b, a.W * b ); }
		public static void Multiply ( float a, ref Vector4 b, out Vector4 result ) { Multiply ( ref b, a, out result ); }
		public static void Divide ( ref Vector4 a, ref Vector4 b, out Vector4 result ) { result = new Vector4 ( a.X / b.X, a.Y / b.Y, a.Z / b.Z, a.W / b.W ); }
		public static void Divide ( ref Vector4 a, float b, out Vector4 result ) { result = new Vector4 ( a.X / b, a.Y / b, a.Z / b, a.W / b ); }

		public static float Dot ( Vector4 v1, Vector4 v2 ) { float result; Dot ( ref v1, ref v2, out result ); return result; }
		public static Vector4 Cross ( Vector4 v1, Vector4 v2, Vector4 v3 ) { Vector4 result; Cross ( ref v1, ref v2, ref v3, out result ); return result; }

		public static void Dot ( ref Vector4 v1, ref Vector4 v2, out float result )
		{ result = v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z + v1.W * v2.W; }
		public static void Cross ( ref Vector4 v1, ref Vector4 v2, ref Vector4 v3, out Vector4 result )
		{
			result = new Vector4 (
				v1.W * v3.Y - v2.W * v3.Z + v1.W * v3.W,
				-v1.W * v3.X + v1.X * v1.Y * v3.Z - v2.W * v3.W,
				v2.W * v3.X - v1.X * v1.Y * v3.Y + v1.W * v3.W,
				-v1.W * v3.X + v2.W * v3.Y - v1.W - v3.Z
			);
		}
		public static Vector4 Transform ( Vector2 position, Matrix4x4 matrix )
		{
			Vector4 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}
		public static Vector4 Transform ( Vector3 position, Matrix4x4 matrix )
		{
			Vector4 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}
		public static Vector4 Transform ( Vector4 position, Matrix4x4 matrix )
		{
			Vector4 result;
			Transform ( ref position, ref matrix, out result );
			return result;
		}

		public static void Transform ( ref Vector2 position, ref Matrix4x4 matrix, out Vector4 result )
		{
			result = new Vector4 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + matrix.M41,
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + matrix.M42,
				( position.X * matrix.M13 ) + ( position.Y * matrix.M23 ) + matrix.M43,
				( position.X * matrix.M14 ) + ( position.Y * matrix.M24 ) + matrix.M44
			);
		}
		public static void Transform ( ref Vector3 position, ref Matrix4x4 matrix, out Vector4 result )
		{
			result = new Vector4 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + ( position.Z * matrix.M31 ) + matrix.M41,
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + ( position.Z * matrix.M32 ) + matrix.M42,
				( position.X * matrix.M13 ) + ( position.Y * matrix.M23 ) + ( position.Z * matrix.M33 ) + matrix.M43,
				( position.X * matrix.M14 ) + ( position.Y * matrix.M24 ) + ( position.Z * matrix.M34 ) + matrix.M44
			);
		}
		public static void Transform ( ref Vector4 position, ref Matrix4x4 matrix, out Vector4 result )
		{
			result = new Vector4 (
				( position.X * matrix.M11 ) + ( position.Y * matrix.M21 ) + ( position.Z * matrix.M31 ) + ( position.W * matrix.M41 ),
				( position.X * matrix.M12 ) + ( position.Y * matrix.M22 ) + ( position.Z * matrix.M32 ) + ( position.W * matrix.M42 ),
				( position.X * matrix.M13 ) + ( position.Y * matrix.M23 ) + ( position.Z * matrix.M33 ) + ( position.W * matrix.M43 ),
				( position.X * matrix.M14 ) + ( position.Y * matrix.M24 ) + ( position.Z * matrix.M34 ) + ( position.W * matrix.M44 )
			);
		}

		public static float Distance ( Vector4 v1, Vector4 v2 ) { float result; Distance ( ref v1, ref v2, out result ); return result; }
		public static float DistanceSquared ( Vector4 v1, Vector4 v2 ) { float result; DistanceSquared ( ref v1, ref v2, out result ); return result; }

		public static void Distance ( ref Vector4 v1, ref Vector4 v2, out float result ) { result = ( v2 - v1 ).Length; }
		public static void DistanceSquared ( ref Vector4 v1, ref Vector4 v2, out float result ) { result = ( v2 - v1 ).LengthSquared; }

		public static Vector4 Max ( Vector4 v1, Vector4 v2 ) { Vector4 result; Max ( ref v1, ref v2, out result ); return result; }
		public static Vector4 Min ( Vector4 v1, Vector4 v2 ) { Vector4 result; Min ( ref v1, ref v2, out result ); return result; }
		public static Vector4 Clamp ( Vector4 v1, Vector4 v2, Vector4 v3 ) { Vector4 result; Clamp ( ref v1, ref v2, ref v3, out result ); return result; }
		public static Vector4 Absolute ( Vector4 v ) { Vector4 result; Absolute ( ref v, out result ); return result; }
		public static Vector4 Barycentric ( Vector4 value1, Vector4 value2, Vector4 value3, float amount1, float amount2 )
		{
			Vector4 result;
			Barycentric ( ref value1, ref value2, ref value3, amount1, amount2, out result );
			return result;
		}
		public static Vector4 CatmullRom ( Vector4 value1, Vector4 value2, Vector4 value3, Vector4 value4, float amount )
		{
			Vector4 result;
			CatmullRom ( ref value1, ref value2, ref value3, ref value4, amount, out result );
			return result;
		}
		public static Vector4 Reflect ( Vector4 vector, Vector4 normal )
		{
			Vector4 result;
			Reflect ( ref vector, ref normal, out result );
			return result;
		}
		public static Vector4 Hermite ( Vector4 value1, Vector4 tangent1, Vector4 value2, Vector4 tangent2, float amount )
		{
			Vector4 result;
			Hermite ( ref value1, ref tangent1, ref value2, ref tangent2, amount, out result );
			return result;
		}
		public static Vector4 Lerp ( Vector4 value1, Vector4 value2, float amount )
		{
			Vector4 result;
			Lerp ( ref value1, ref value2, amount, out result );
			return result;
		}
		public static Vector4 SmoothStep ( Vector4 value1, Vector4 value2, float amount )
		{
			Vector4 result;
			SmoothStep ( ref value1, ref value2, amount, out result );
			return result;
		}

		public static void Max ( ref Vector4 v1, ref Vector4 v2, out Vector4 result )
		{ result = new Vector4 ( Math.Max ( v1.X, v2.X ), Math.Max ( v1.Y, v2.Y ), Math.Max ( v1.Z, v2.Z ), Math.Max ( v1.W, v2.W ) ); }
		public static void Min ( ref Vector4 v1, ref Vector4 v2, out Vector4 result )
		{ result = new Vector4 ( Math.Min ( v1.X, v2.X ), Math.Min ( v1.Y, v2.Y ), Math.Min ( v1.Z, v2.Z ), Math.Min ( v1.W, v2.W ) ); }
		public static void Clamp ( ref Vector4 v1, ref Vector4 v2, ref Vector4 v3, out Vector4 result ) { result = Max ( v2, Min ( v1, v3 ) ); }
		public static void Absolute ( ref Vector4 v, out Vector4 result )
		{ result = new Vector4 ( Math.Abs ( v.X ), Math.Abs ( v.Y ), Math.Abs ( v.Z ), Math.Abs ( v.W ) ); }
		public static void Barycentric ( ref Vector4 value1, ref Vector4 value2, ref Vector4 value3, float amount1, float amount2, out Vector4 result )
		{
			result = new Vector4 (
				MathHelper.Barycentric ( value1.X, value2.X, value3.X, amount1, amount2 ),
				MathHelper.Barycentric ( value1.Y, value2.Y, value3.Y, amount1, amount2 ),
				MathHelper.Barycentric ( value1.Z, value2.Z, value3.Z, amount1, amount2 ),
				MathHelper.Barycentric ( value1.W, value2.W, value3.W, amount1, amount2 )
			);
		}
		public static void CatmullRom ( ref Vector4 value1, ref Vector4 value2, ref Vector4 value3, ref Vector4 value4, float amount, out Vector4 result )
		{
			result = new Vector4 (
				MathHelper.CatmullRom ( value1.X, value2.X, value3.X, value4.X, amount ),
				MathHelper.CatmullRom ( value1.Y, value2.Y, value3.Y, value4.Y, amount ),
				MathHelper.CatmullRom ( value1.Z, value2.Z, value3.Z, value4.Z, amount ),
				MathHelper.CatmullRom ( value1.W, value2.W, value3.W, value4.W, amount )
			);
		}
		public static void Reflect ( ref Vector4 vector, ref Vector4 normal, out Vector4 result )
		{
			float val = 2.0f * ( ( vector.X * normal.X ) + ( vector.Y * normal.Y ) );
			result = new Vector4 ( vector.X - ( normal.X * val ), vector.Y - ( normal.Y * val ), vector.Z - ( normal.Z * val ), vector.W - ( normal.W * val ) );
		}
		public static void Hermite ( ref Vector4 value1, ref Vector4 tangent1, ref Vector4 value2, ref Vector4 tangent2, float amount, out Vector4 result )
		{
			result = new Vector4 (
				MathHelper.Hermite ( value1.X, tangent1.X, value2.X, tangent2.X, amount ),
				MathHelper.Hermite ( value1.Y, tangent1.Y, value2.Y, tangent2.Y, amount ),
				MathHelper.Hermite ( value1.Z, tangent1.Z, value2.Z, tangent2.Z, amount ),
				MathHelper.Hermite ( value1.W, tangent1.W, value2.W, tangent2.W, amount )
			);
		}
		public static void Lerp ( ref Vector4 value1, ref Vector4 value2, float amount, out Vector4 result )
		{
			result = new Vector4 (
				MathHelper.Lerp ( value1.X, value2.X, amount ),
				MathHelper.Lerp ( value1.Y, value2.Y, amount ),
				MathHelper.Lerp ( value1.Z, value2.Z, amount ),
				MathHelper.Lerp ( value1.W, value2.W, amount )
			);
		}
		public static void SmoothStep ( ref Vector4 value1, ref Vector4 value2, float amount, out Vector4 result )
		{
			result = new Vector4 (
				MathHelper.SmoothStep ( value1.X, value2.X, amount ),
				MathHelper.SmoothStep ( value1.Y, value2.Y, amount ),
				MathHelper.SmoothStep ( value1.Z, value2.Z, amount ),
				MathHelper.SmoothStep ( value1.W, value2.W, amount )
			);
		}
	}
}
