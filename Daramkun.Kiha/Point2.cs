﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Daramkun.Kiha
{
	[StructLayout ( LayoutKind.Sequential )]
	public struct Point2 : IEquatable<Point2>
	{
		public static implicit operator Vector2 ( Point2 p ) { return new Vector2 ( p.X, p.Y ); }
		public static implicit operator Point2 ( Vector2 p ) { return new Point2 ( p ); }

		public int X, Y;

		public Point2 ( int x, int y ) { X = x; Y = y; }
		public Point2 ( Vector2 v ) : this ( ( int ) v.X, ( int ) v.Y ) { }

		public override string ToString () { return string.Format ( "{{X:{0}, Y:{1}}}", X, Y ); }

		public bool Equals ( Point2 other ) { return X == other.X && Y == other.Y; }
	}
}
